package xyz.lbartolic.e_gluko.webservices.api.repositories;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.lbartolic.e_gluko.models.User;
import xyz.lbartolic.e_gluko.webservices.api.ApiClient;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.UserInterface;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.UserResponse;

/**
 * Created by asus on 14.08.16..
 */
public class UserRepository {
    private static final UserInterface userInterface = ApiClient.getClient()
            .create(UserInterface.class);

    public UserRepository() {

    }

    public static void authenticateUser(String[] credentials, final UserResponse userResponse) {
        Call<User> call = userInterface.postAuth(credentials[0], credentials[1]);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    userResponse.onSuccess(user);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    userResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userResponse.onFailure(t);
            }
        });
    }
}

package xyz.lbartolic.e_gluko.models.Statistics;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import xyz.lbartolic.e_gluko.models.HealthGoal;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.Statistics.Glucose.GlucoseStats;
import xyz.lbartolic.e_gluko.models.Statistics.Insulin.InsulinStats;

/**
 * Created by asus on 07.09.17..
 */
public class TotalStats {
    @SerializedName("date_from")
    String dateFrom;
    @SerializedName("date_to")
    String dateTo;
    @SerializedName("stats")
    StatsByType statsByType;
    @SerializedName("health_goals")
    private List<HealthGoal> healthGoals;

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public StatsByType getStatsByType() {
        return statsByType;
    }

    public List<HealthGoal> getHealthGoals() {
        return healthGoals;
    }
}

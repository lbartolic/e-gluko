package xyz.lbartolic.e_gluko.models.HealthLogsList;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import xyz.lbartolic.e_gluko.models.HealthGoal;
import xyz.lbartolic.e_gluko.models.HealthLog;

/**
 * Created by asus on 03.09.17..
 */
public class ListData {
    private static final String PREFS_KEY = "health_logs_list_data";

    @SerializedName("date")
    private String date;
    @SerializedName("logs")
    private List<HealthLog> healthLogs;
    @SerializedName("health_goal")
    private HealthGoal healthGoal;
    @SerializedName("stats")
    private JsonObject stats;
    //private BasicTotalStats stats;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public String getDate() {
        return date;
    }

    public List<HealthLog> getHealthLogs() {
        return healthLogs;
    }

    public HealthGoal getHealthGoal() {
        return healthGoal;
    }

    public JsonObject getStats() {
        return stats;
    }
}

package xyz.lbartolic.e_gluko.activities;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.utils.DateUtil;

public class TrendsActivity extends AppCompatActivity implements OnChartValueSelectedListener {
    private CombinedChart combinedChart;
    private HealthLogsList logs;
    private List<HealthLog> glucoseLogs; //temp

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trends);

        combinedChart = (CombinedChart) findViewById(R.id.chart);

        Bundle extras = getIntent().getExtras();
        logs = new Gson().fromJson(extras.getString("HEALTH_LOGS"), HealthLogsList.class);

        glucoseLogs = new ArrayList<>();
        for(ListData data : logs.getListData()) {
            List<HealthLog> healthLogs = data.getHealthLogs();
            for(HealthLog healthLog : healthLogs) {
                if (healthLog.getLogType().getType().equals(HealthLogType.TYPE_GLUCOSE)) {
                    glucoseLogs.add(healthLog);
                    Log.d("ADD", "add");
                }
            }
        }

        Date dateFrom = DateUtil.convertStringToDate(logs.getDateFrom(), DateUtil.DB_DATE_FORMAT);
        Date dateTo = DateUtil.convertStringToDate(logs.getDateTo(), DateUtil.DB_DATE_FORMAT);
        List<Float> averages = new ArrayList<>();
        List<List<Float>> glucoseDailyLogs = new ArrayList<>();
        List<Date> dates = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        Date tmpDateTo = new Date(dateTo.getTime() + TimeUnit.DAYS.toMillis(1));
        cal.setTime(dateFrom);
        while (cal.getTime().before(tmpDateTo)) {
            dates.add(cal.getTime());
            cal.add(Calendar.DATE, 1);
        }
        for (Date date : dates) {
            Log.d("DATE", date.toString());
            Float average = null;
            List<Float> dayGlucoseLogs = new ArrayList<>();
            int count = 0;
            int dateIndex = 0;
            for (HealthLog log : glucoseLogs) {
                String logDate = DateUtil.formatDateString(log.getLogTs(), DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_DATE_FORMAT_DATE);
                String tmpDate = DateUtil.convertDateToString(date, DateUtil.APP_DATE_FORMAT_DATE);
                if (logDate.equals(tmpDate)) {
                    if (average == null) average = log.getValue();
                    else average += log.getValue();
                    count++;
                    dayGlucoseLogs.add(log.getValue());
                }
            }
            if (average != null) average = average/count;
            averages.add(average);
            glucoseDailyLogs.add(dayGlucoseLogs);
            dateIndex++;
        }

        ArrayList<Entry> entries = new ArrayList<>();
        int index = 0;
        for(Float average : averages) {
            if (average != null) {
                entries.add(new Entry(index, average));
            }
            else {
                //entries.add(new Entry(index, 0));
            }
            index++;
        }

        ArrayList<Entry> entriesScatter = new ArrayList<>();
        index = 0;
        for (List<Float> dayLogs : glucoseDailyLogs) {
            for(Float log : dayLogs) {
                entriesScatter.add(new Entry(index, log));
            }
            index++;
        }



        //Log.d("CHART_DATA", glucoseLogs.get(0).getValue()+"");

        /*ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Long> xValues = new ArrayList<>();
        //Calendar c = Calendar.getInstance();
        int index = 0;
        for(HealthLog glucoseLog : glucoseLogs) {
            //c.setTime(DateUtil.convertStringToDate(glucoseLog.getLogTs(), DateUtil.DB_TIMESTAMP_FORMAT));
            //int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            Date date = DateUtil.convertStringToDate(glucoseLog.getLogTs(), DateUtil.DB_TIMESTAMP_FORMAT);
            entries.add(new Entry(index, glucoseLog.getValue()));
            xValues.add(date.getTime());
            index++;
        }*/

        Log.d("DATES", dates.size()+"");
        Log.d("ENTRIES", entries.size()+"");

        LineDataSet datasetLine = new LineDataSet(entries, "Razina glukoze");
        ScatterDataSet datasetScatter = new ScatterDataSet(entriesScatter, "Mjerenja");
        datasetLine.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
        datasetLine.setCircleColor(ContextCompat.getColor(this, R.color.colorPrimary));
        datasetLine.setLineWidth(3);
        datasetLine.setCircleRadius(4);
        datasetLine.setValueTextSize(0);
        datasetLine.setHighLightColor(ContextCompat.getColor(this, R.color.colorTextPrimary));
        datasetScatter.setValueTextSize(0);
        datasetScatter.setHighLightColor(ContextCompat.getColor(this, R.color.colorTextPrimary));

        if (entries.size() > 0) {
            LineData data = new LineData(datasetLine);
            ScatterData scatterData = new ScatterData(datasetScatter);
            CombinedData combinedData = new CombinedData();
            combinedData.setData(data);
            combinedData.setData(scatterData);
            combinedChart.setData(combinedData);
            combinedChart.setDescription(null);
            combinedChart.setOnChartValueSelectedListener(this);
            XAxis xAxis = combinedChart.getXAxis();
            YAxis leftAxis = combinedChart.getAxisLeft();
            YAxis right = combinedChart.getAxisRight();
            xAxis.setValueFormatter(new DayXAxisValueFormatter(dateFrom, dateTo));
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setLabelCount(entries.size(), true);
            leftAxis.setAxisMinimum(0);
            leftAxis.setLabelCount(5, true);
            leftAxis.setGridColor(Color.parseColor("#dddddd"));
            xAxis.setGridColor(Color.parseColor("#dddddd"));
            xAxis.setAxisLineColor(Color.parseColor("#dddddd"));
            right.setEnabled(false);
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Toast.makeText(TrendsActivity.this, String.format("%.1f", h.getY()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }

    public class DayXAxisValueFormatter implements IAxisValueFormatter {
        private Date dateFrom;
        private Date dateTo;
        private List<String> values;
        private Calendar cal;

        public DayXAxisValueFormatter(Date dateFrom, Date dateTo) {
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
            this.values = new ArrayList<>();
            this.cal = Calendar.getInstance();
            Date tmpDateTo = new Date(dateTo.getTime() + TimeUnit.DAYS.toMillis(1));
            cal.setTime(dateFrom);
            while (cal.getTime().before(tmpDateTo)) {
                String tempDate = DateUtil.convertDateToString(cal.getTime(), DateUtil.APP_DATE_FORMAT_DATE_DM);
                values.add(tempDate);
                cal.add(Calendar.DATE, 1);
                Log.d("DATE", tempDate);
            }
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            return values.get(((int) value));
        }
    }
}

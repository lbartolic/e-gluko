package xyz.lbartolic.e_gluko.webservices.api.interfaces;

import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;

/**
 * Created by asus on 17.08.16..
 */
public interface HealthLogsListResponse {
    void onSuccess(HealthLogsList healthLogsList);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}

package xyz.lbartolic.e_gluko.webservices.api.interfaces;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import xyz.lbartolic.e_gluko.models.User;

/**
 * Created by asus on 13.08.16..
 */
public interface UserInterface {
    @GET("{api_key}")
    Call<User> getUser(@Path("api_key") String apiKey);

    @FormUrlEncoded
    @POST("auth")
    Call<User> postAuth(@Field("email") String email, @Field("password") String password);
}

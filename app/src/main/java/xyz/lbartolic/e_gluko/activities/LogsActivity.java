package xyz.lbartolic.e_gluko.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.adapters.LogsListDayPagerAdapter;
import xyz.lbartolic.e_gluko.fragments.LogsListDayFragment;
import xyz.lbartolic.e_gluko.fragments.TrendsFragment;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogCreateData;
import xyz.lbartolic.e_gluko.models.HealthLogLabel;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.BasicTotalStats;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;
import xyz.lbartolic.e_gluko.models.InsulinType;
import xyz.lbartolic.e_gluko.models.User;
import xyz.lbartolic.e_gluko.utils.DateUtil;
import xyz.lbartolic.e_gluko.utils.JsonParseUtil;
import xyz.lbartolic.e_gluko.utils.UnitsUtil;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogCreateDataResponse;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogResponse;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogsListResponse;
import xyz.lbartolic.e_gluko.webservices.api.repositories.HealthLogRepository;

public class LogsActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, Validator.ValidationListener {
    private Toolbar toolbar;
    private EditText etDate;
    private Button btnLogsPrev;
    private Button btnLogsNext;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private LinearLayout llPeriodStats;
    private LinearLayout llStatsGlucoseAvg;
    private LinearLayout llStatsGlucoseMin;
    private LinearLayout llStatsGlucoseMax;
    private LinearLayout llStatsAvgDailyBasal;
    private LinearLayout llStatsAvgDailyBolus;
    private LinearLayout llStatsAvgDailySteps;
    private LinearLayout llStatsAvgDailyCalories;
    private LinearLayout llStatsTotalActivity;
    private LinearLayout llStatsAvgHeartRate;
    private LinearLayout llStatsTotalSleep;
    private LinearLayout llStatsAvgSleepQuality;
    private HorizontalScrollView hsvPeriodStats;
    private ObjectAnimator totalStatsAnimator;
    private TabLayout tlDay;
    private ViewPager dayPager;
    private PagerAdapter pagerAdapter;
    private ProgressBar pbLogs;
    private String currentDate;
    private HealthLogsList healthLogsList;
    private RelativeLayout rlStatsTrendsHolder;
    private TrendsFragment trendsFragment;
    private FloatingActionButton fabNewLog;
    @NotEmpty(message = "Potrebno")
    private EditText etLogDate;
    @NotEmpty(message = "Potrebno")
    private EditText etLogTime;
    @NotEmpty(message = "Potrebno")
    private EditText etValue;
    private Validator validator;
    private ProgressDialog progressDialog;
    private HealthLogCreateData createData;
    private String createLogType;
    private Spinner createSpinnerLabel;
    private Dialog dialogNewLog;
    private Spinner spinnerInsulinType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);
        currentDate = DateUtil.getCurrentDate();
        initUI();
        setLogsList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    private void setLogsList() {
        showLoading(true);
        HealthLogRepository.getLogsList(User.getAuthUserApiToken(this), currentDate, "asc", new HealthLogsListResponse() {
            @Override
            public void onSuccess(final HealthLogsList healthLogsList) {
                hideLoading();
                LogsActivity.this.healthLogsList = healthLogsList;
                JsonObject totalPeriodStats = healthLogsList.getStats();
                setLogsListPaging(totalPeriodStats);

                String dateFrom = DateUtil.formatDateString(healthLogsList.getDateFrom(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_DATE);
                String dateTo = DateUtil.formatDateString(healthLogsList.getDateTo(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_DATE);
                etDate.setText(String.format("%s - %s", dateFrom, dateTo));


            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void setLogsListPaging(JsonObject totalPeriodStats) {
        setBasicTotalStats(llPeriodStats, totalPeriodStats);
        pagerAdapter = new LogsListDayPagerAdapter(getSupportFragmentManager(), getApplicationContext(), healthLogsList);
        dayPager.setAdapter(pagerAdapter);
        tlDay.setupWithViewPager(dayPager);
        new Thread(new Runnable() {
            @Override
            public void run() {
                setTrendsFragment();
            }
        }).start();
    }

    private void setTrendsFragment() {
        trendsFragment = new TrendsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("HEALTH_LOGS", JsonParseUtil.convertObjectToJson(healthLogsList));
        trendsFragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.charts, trendsFragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_logs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.itemShowTrends:
                Intent intent = new Intent(LogsActivity.this, TrendsActivity.class);
                intent.putExtra("HEALTH_LOGS", JsonParseUtil.convertObjectToJson(healthLogsList));
                startActivity(intent);
                break;
        }
        return true;
    }

    private void initNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                Intent intent;
                switch (id) {
                    /*case R.id.navLogs:
                        intent = new Intent(LogsActivity.this, LogsActivity.class);
                        startActivityForResult(intent, 2);
                        break;*/
                    case R.id.navTrendsStats:
                        intent = new Intent(LogsActivity.this, StatsActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.navHealthGoals:
                        break;
                    case R.id.navLogOut:
                        logout();
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        View navHeader = navigationView.getHeaderView(0);
        TextView navFullName = (TextView) navHeader.findViewById(R.id.navHeaderFullName);
        TextView navEmail = (TextView) navHeader.findViewById(R.id.navHeaderEmail);
        navFullName.setText(User.getAuthUserFullName(this));
        navEmail.setText(User.getAuthUserEmail(this));
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    private void logout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText(R.string.msg_log_out);
        ((TextView) dialog.findViewById(R.id.tvTitle)).setText(R.string.action_sign_out);
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.removeUserPrefs(LogsActivity.this);
                Intent intent;
                intent = new Intent(LogsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    private void showLoading(boolean hide) {
        if (hide) {
            dayPager.setVisibility(View.GONE);
            rlStatsTrendsHolder.setVisibility(View.GONE);
            tlDay.setVisibility(View.GONE);
        }
        pbLogs.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        pbLogs.setVisibility(View.GONE);
        rlStatsTrendsHolder.setVisibility(View.VISIBLE);
        tlDay.setVisibility(View.VISIBLE);
        dayPager.setVisibility(View.VISIBLE);
    }

    private void initUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.activity_logs_title);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        initNavigationDrawer();
        pbLogs = (ProgressBar) findViewById(R.id.pbLogs);
        btnLogsPrev = (Button) findViewById(R.id.btnLogsPrev);
        btnLogsNext = (Button) findViewById(R.id.btnLogsNext);
        etDate = (EditText) findViewById(R.id.etDate);
        llPeriodStats = (LinearLayout) findViewById(R.id.llPeriodStats);
        llStatsAvgDailyBasal = (LinearLayout) findViewById(R.id.llStatsAvgDailyBasal);
        llStatsAvgDailyBolus = (LinearLayout) findViewById(R.id.llStatsAvgDailyBolus);
        llStatsAvgDailySteps = (LinearLayout) findViewById(R.id.llStatsAvgDailySteps);
        llStatsAvgDailyCalories = (LinearLayout) findViewById(R.id.llStatsAvgDailyCalories);
        llStatsAvgHeartRate = (LinearLayout) findViewById(R.id.llStatsAvgHeartRate);
        llStatsAvgSleepQuality = (LinearLayout) findViewById(R.id.llStatsAvgSleepQuality);
        llStatsGlucoseAvg = (LinearLayout) findViewById(R.id.llStatsGlucoseAvg);
        llStatsGlucoseMax = (LinearLayout) findViewById(R.id.llStatsGlucoseMax);
        llStatsGlucoseMin = (LinearLayout) findViewById(R.id.llStatsGlucoseMin);
        llStatsTotalActivity = (LinearLayout) findViewById(R.id.llStatsTotalActivity);
        llStatsTotalSleep = (LinearLayout) findViewById(R.id.llStatsTotalSleep);
        hsvPeriodStats = (HorizontalScrollView) findViewById(R.id.hsvPeriodStats);
        rlStatsTrendsHolder = (RelativeLayout) findViewById(R.id.rlStatsTrendsHolder);
        fabNewLog = (FloatingActionButton) findViewById(R.id.fabNewLog);
        fabNewLog.setOnClickListener(this);
        dayPager = (ViewPager) findViewById(R.id.viewPager);
        tlDay = (TabLayout) findViewById(R.id.tlDay);
        dayPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlDay));
        tlDay.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                dayPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        btnLogsPrev.setOnClickListener(this);
        btnLogsNext.setOnClickListener(this);
        etDate.setOnClickListener(this);
        etDate.setFocusable(false);
        etDate.setClickable(true);
    }

    private void animateTotalStats(final int direction) {
        int position;
        if (direction == 0) {
            position = llPeriodStats.getWidth() - hsvPeriodStats.getWidth();
        } else {
            position = 0;
        }
        totalStatsAnimator = ObjectAnimator.ofInt(hsvPeriodStats, "scrollX", position);
        totalStatsAnimator.setDuration(20000);
        totalStatsAnimator.setInterpolator(new LinearInterpolator());
        totalStatsAnimator.start();
        totalStatsAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (direction == 0) animateTotalStats(1);
                else animateTotalStats(0);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                Log.d("anim", "cancel");
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        hsvPeriodStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalStatsAnimator.end();
                totalStatsAnimator.cancel();
            }
        });
    }

    @Override
    public void onClick(View view) {
        Date currentDate = DateUtil.convertStringToDate(this.currentDate, DateUtil.DB_DATE_FORMAT);
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                LogsActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                LogsActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        switch (view.getId()) {
            case R.id.etDate:
                dpd.show(getFragmentManager(), "datepicker_logs_list");
                break;
            case R.id.btnLogsPrev:
                assert currentDate != null;
                this.currentDate = DateUtil.convertDateToString(new Date(currentDate.getTime() - 7 * 24 * 3600 * 1000), DateUtil.DB_DATE_FORMAT).toString();
                setLogsList();
                break;
            case R.id.btnLogsNext:
                this.currentDate = DateUtil.convertDateToString(new Date(currentDate.getTime() + 7 * 24 * 3600 * 1000), DateUtil.DB_DATE_FORMAT).toString();
                setLogsList();
                break;
            case R.id.fabNewLog:
                showNewLogOptionsDialog();
                break;
            case R.id.etLogDate:
                dpd.show(getFragmentManager(), "datepicker_new_log");
                break;
            case R.id.etLogTime:
                tpd.show(getFragmentManager(), "timepicker_new_log");
                break;
            default:
                break;
        }
    }

    private void showNewLogOptionsDialog() {
        final Dialog dialog = new Dialog(LogsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_new_log_type);
        Button glucoseLog = (Button) dialog.findViewById(R.id.btnGlucose);
        Button insulinLog = (Button) dialog.findViewById(R.id.btnInsulin);
        glucoseLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showNewLogDialog(HealthLogType.TYPE_GLUCOSE);
            }
        });
        insulinLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showNewLogDialog(HealthLogType.TYPE_INSULIN);
            }
        });
        (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showNewLogDialog(final String logType) {
        dialogNewLog = new Dialog(LogsActivity.this);
        dialogNewLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNewLog.setContentView(R.layout.dialog_new_log);
        final TextView tvLogType = (TextView) dialogNewLog.findViewById(R.id.tvLogType);
        final FrameLayout flLogTypeExtra = (FrameLayout) dialogNewLog.findViewById(R.id.flLogTypeExtra);
        flLogTypeExtra.removeAllViews();
        HealthLogRepository.getCreateData(User.getAuthUserApiToken(this), new HealthLogCreateDataResponse() {
            @Override
            public void onSuccess(HealthLogCreateData createData) {
                LogsActivity.this.createData = createData;
                LogsActivity.this.createLogType = logType;
                Log.d("CREATE_DATA", createData.getInsulinTypes().get(0) + " " + createData.getLabels().get(0));
                etValue = (EditText) dialogNewLog.findViewById(R.id.etValue);
                etLogDate = (EditText) dialogNewLog.findViewById(R.id.etLogDate);
                etLogTime = (EditText) dialogNewLog.findViewById(R.id.etLogTime);
                etLogDate.setOnClickListener(LogsActivity.this);
                etLogTime.setOnClickListener(LogsActivity.this);
                etLogDate.setFocusable(false);
                etLogDate.setClickable(true);
                etLogTime.setFocusable(false);
                etLogTime.setClickable(true);
                validator = new Validator(LogsActivity.this);
                validator.setValidationListener(LogsActivity.this);

                ArrayList<String> labelSpinnerArray = new ArrayList<>();
                ArrayList<String> insulinTypeSpinnerArray = new ArrayList<>();
                for (HealthLogLabel label : createData.getLabels()) {
                    labelSpinnerArray.add(label.getDisplayValue());
                }
                for (InsulinType type : createData.getInsulinTypes()) {
                    Log.d("TYPE", type.getDisplayValue());
                    insulinTypeSpinnerArray.add(type.getDisplayValue());
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(LogsActivity.this, android.R.layout.simple_spinner_dropdown_item, labelSpinnerArray);
                ArrayAdapter<String> spinerInsulinTypeArrayAdapter = new ArrayAdapter<>(LogsActivity.this, android.R.layout.simple_spinner_dropdown_item, insulinTypeSpinnerArray);

                if (logType.equals(HealthLogType.TYPE_GLUCOSE)) {
                    etValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    tvLogType.setText("Razina glukoze");
                    tvLogType.setTextColor(ContextCompat.getColor(LogsActivity.this, R.color.glucose));
                    LinearLayout llNewLogGlucose = (LinearLayout) getLayoutInflater().inflate(R.layout.extra_new_log_glucose, flLogTypeExtra, false);
                    createSpinnerLabel = (Spinner) llNewLogGlucose.findViewById(R.id.spinnerLabel);
                    createSpinnerLabel.setAdapter(spinnerArrayAdapter);
                    flLogTypeExtra.addView(llNewLogGlucose);
                }
                if (logType.equals(HealthLogType.TYPE_INSULIN)) {
                    etValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                    tvLogType.setText("Doze inzulina");
                    tvLogType.setTextColor(ContextCompat.getColor(LogsActivity.this, R.color.insulin));
                    LinearLayout llNewLogInsulin = (LinearLayout) getLayoutInflater().inflate(R.layout.extra_new_log_insulin, flLogTypeExtra, false);
                    createSpinnerLabel = (Spinner) llNewLogInsulin.findViewById(R.id.spinnerLabel);
                    createSpinnerLabel.setAdapter(spinnerArrayAdapter);
                    spinnerInsulinType = (Spinner) llNewLogInsulin.findViewById(R.id.spinnerInsulinType);
                    spinnerInsulinType.setAdapter(spinerInsulinTypeArrayAdapter);
                    flLogTypeExtra.addView(llNewLogInsulin);
                }

                dialogNewLog.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogNewLog.dismiss();
                    }
                });
                dialogNewLog.findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        validator.validate();
                    }
                });
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
        dialogNewLog.show();
    }

    private void storeHealthLog() {
        progressDialog = ProgressDialog.show(this, null, "Spremanje u tijeku", true, false);
        Float value = Float.valueOf(etValue.getText().toString());
        String date = DateUtil.formatDateString(etLogDate.getText().toString(), DateUtil.APP_DATE_FORMAT_DATE, DateUtil.DB_DATE_FORMAT);
        String time = DateUtil.formatDateString(etLogTime.getText().toString(), DateUtil.APP_TIME_FORMAT, DateUtil.DB_TIME_FORMAT);
        String logTs = String.format("%s %s", date, time);
        String typeKey = createLogType;
        int logLabelIndex = createSpinnerLabel.getSelectedItemPosition();
        int logLabelId = createData.getLabels().get(logLabelIndex).getId();
        Integer insulinTypeId = null;
        Log.d("LOG_TS", logTs);
        Log.d("TYPE_KEY", typeKey);
        Log.d("LOG_LABEL_POS", logLabelIndex + "");
        Log.d("LOG_LABEL", createData.getLabels().get(logLabelIndex).getKey()+"");
        if (createLogType.equals(HealthLogType.TYPE_GLUCOSE)) {}
        if (createLogType.equals(HealthLogType.TYPE_INSULIN)) {
            insulinTypeId = createData.getInsulinTypes().get(spinnerInsulinType.getSelectedItemPosition()).getId();
        }
        HealthLogRepository.postStore(User.getAuthUserApiToken(this), value, logTs, typeKey, logLabelId, insulinTypeId, new HealthLogResponse() {
            @Override
            public void onSuccess(HealthLog healthLog) {
                Toast.makeText(LogsActivity.this, "Novi zapis dodan.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                dialogNewLog.dismiss();
                refreshData(null, true);
            }

            @Override
            public void onError(int status_code) {
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable throwable) {
                progressDialog.dismiss();
            }
        });
    }

    public void refreshData(@Nullable final LogsListDayFragment lldf, final boolean showFragmentLoading) {
        final int currentItem = dayPager.getCurrentItem();
        if (showFragmentLoading) showLoading(false);
        HealthLogRepository.getLogsList(User.getAuthUserApiToken(this), currentDate, "asc", new HealthLogsListResponse() {
            @Override
            public void onSuccess(final HealthLogsList healthLogsList) {
                if (lldf != null) {
                    lldf.stopSwipeRefresh();
                }
                if (showFragmentLoading) hideLoading();

                LogsActivity.this.healthLogsList = healthLogsList;
                JsonObject totalPeriodStats = healthLogsList.getStats();

                setLogsListPaging(totalPeriodStats);

                dayPager.setCurrentItem(currentItem);
            }

            @Override
            public void onError(int status_code) {}

            @Override
            public void onFailure(Throwable throwable) {}
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String tag = view.getTag();
        String dateString = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        if (tag.equals("datepicker_logs_list")) {
            String dateStringFormat = DateUtil.formatDateString(dateString, "yyyy-MM-dd", DateUtil.APP_DATE_FORMAT_LONG);
            etDate.setText(dateStringFormat);
            currentDate = dateString;
            setLogsList();
        }
        if (tag.equals("datepicker_new_log")) {
            String dateStringFormat = DateUtil.formatDateString(dateString, "yyyy-MM-dd", DateUtil.APP_DATE_FORMAT_DATE);
            etLogDate.setText(dateStringFormat);
        }
    }

    public void setBasicTotalStats(ViewGroup holder, JsonObject totalPeriodStats) {
        holder.removeAllViews();
        ArrayList<String[]> keysArr = BasicTotalStats.getKeyWithout("activity_daily_avg");
        String[] resourceKeys = getResources().getStringArray(R.array.basic_total_stats_keys);
        String[] titles = getResources().getStringArray(R.array.basic_total_stats_titles);
        for(String[] keys : keysArr) {
            for (String key : keys) {
                JsonObject statsDescriptiveValue = new JsonObject();
                if (JsonParseUtil.isSet(totalPeriodStats.get(key))) {
                    if (totalPeriodStats.get(key).isJsonObject()) {
                        statsDescriptiveValue = totalPeriodStats.get(key).getAsJsonObject();
                    } else {
                        statsDescriptiveValue.addProperty("value", totalPeriodStats.get(key).getAsFloat());
                    }
                }
                int titleIndex = Arrays.asList(resourceKeys).indexOf(key);
                String type = BasicTotalStats.getStatsTypeForKey(key);
                String formatType = BasicTotalStats.getFormatTypeForKey(key);
                setStatsDescriptiveView(statsDescriptiveValue, holder, type, formatType, titles[titleIndex]);
            }
        }
    }

    public static Integer[] getGoalDiffViewValues(Float value, String desc, String type) {
        Integer indicator = null;
        Integer indicatorColor = null;

        if (value != null) {
            if (type.equals(HealthLogType.TYPE_GLUCOSE)) {
                if (desc != null) {
                    indicator = R.drawable.ic_check_circle_black_24dp;
                    indicatorColor = R.color.success;
                    if (desc.equals("high")) {
                        indicator = R.drawable.ic_arrow_upward_black_24dp;
                        indicatorColor = R.color.danger;
                    }
                    if (desc.equals("low")) {
                        indicator = R.drawable.ic_arrow_downward_black_24dp;
                        indicatorColor = R.color.warning;
                    }
                }
            }
            if (type.equals(HealthLogType.TYPE_INSULIN)) {
                if (desc != null) {
                    indicator = R.drawable.ic_adjust_black_24dp;
                    indicatorColor = R.color.colorTextPrimary;
                    if (desc.equals("high")) {
                        indicator = R.drawable.ic_arrow_upward_black_24dp;
                    }
                    if (desc.equals("low")) {
                        indicator = R.drawable.ic_arrow_downward_black_24dp;
                    }
                }
            }
            if (type.equals(HealthLogType.TYPE_STEP) || type.equals(HealthLogType.TYPE_CALORIE)) {
                if (desc != null) {
                    indicator = R.drawable.ic_adjust_black_24dp;
                    indicatorColor = R.color.success;
                    if (desc.equals("high")) {
                        indicator = R.drawable.ic_arrow_upward_black_24dp;
                        indicatorColor = R.color.success;
                    }
                    if (desc.equals("low")) {
                        indicator = R.drawable.ic_arrow_downward_black_24dp;
                        indicatorColor = R.color.danger;
                    }
                }
            }
            if (type.equals(HealthLogType.TYPE_SLEEP) || type.equals(HealthLogType.TYPE_PHYSICAL_ACTIVITY)) {
                if (desc != null) {
                    indicator = R.drawable.ic_adjust_black_24dp;
                    indicatorColor = R.color.success;
                    if (desc.equals("high")) {
                        indicator = R.drawable.ic_arrow_upward_black_24dp;
                        indicatorColor = R.color.success;
                    }
                    if (desc.equals("low")) {
                        indicator = R.drawable.ic_arrow_downward_black_24dp;
                        indicatorColor = R.color.danger;
                    }
                }
            }
        }
        return new Integer[]{indicator, indicatorColor};
    }

    public void setStatsDescriptiveView(JsonObject descriptiveValue, View holder, String type, String formatType, String title) {
        Float value = null;
        String valueStr = "-";
        String desc = null;
        Float deviationPerc = null;

        if (descriptiveValue != null && !descriptiveValue.isJsonNull()) {
            if (JsonParseUtil.isSet(descriptiveValue.get("value"))) {
                value = descriptiveValue.get("value").getAsFloat();
            }
            if (JsonParseUtil.isSet(descriptiveValue.get("log"))) {
                value = descriptiveValue.get("log").getAsJsonObject().get("value").getAsFloat();
            }
            if (JsonParseUtil.isSet(descriptiveValue.get("desc"))) {
                desc = descriptiveValue.get("desc").getAsString();
            }
            if (JsonParseUtil.isSet(descriptiveValue.get("deviation_percentage"))) {
                deviationPerc = descriptiveValue.get("deviation_percentage").getAsFloat();
            }
        }

        View statsItem = getLayoutInflater().inflate(R.layout.item_period_stats, (ViewGroup) holder, false);

        if (value != null) {
            if (formatType.equals(HealthLogType.FORMAT_TYPE_TIME)) {
                valueStr = UnitsUtil.formatHmsFromMillis(value.intValue());
            }
            else {
                valueStr = UnitsUtil.getValueFormatted(value);
            }
        }

        Integer[] viewValues = getGoalDiffViewValues(value, desc, type);
        Integer indicator = viewValues[0];
        Integer indicatorColor = viewValues[1];

        TextView tvValue = (TextView) statsItem.findViewById(R.id.tvValue);
        TextView tvTitle = (TextView) statsItem.findViewById(R.id.tvTitle);
        TextView tvDeviationPerc = (TextView) statsItem.findViewById(R.id.tvDeviationPerc);
        ImageView ivIndicator = (ImageView) statsItem.findViewById(R.id.ivIndicator);

        tvValue.setText(valueStr);
        tvTitle.setText(title);
        if (deviationPerc != null && deviationPerc != 0) {
            tvDeviationPerc.setText(deviationPerc + "%");
            tvDeviationPerc.setVisibility(View.VISIBLE);
        }
        if (indicator != null) {
            ivIndicator.setImageResource(indicator);
            ivIndicator.setColorFilter(this.getResources().getColor(indicatorColor));
        } else {
            ivIndicator.setVisibility(View.GONE);
        }

        ((ViewGroup) holder).addView(statsItem);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String timeString = hourOfDay + ":" + minute;
        String timeStringFormat = DateUtil.formatDateString(timeString, "HH:mm", DateUtil.APP_TIME_FORMAT);
        etLogTime.setText(timeStringFormat);
    }

    @Override
    public void onValidationSucceeded() {
        storeHealthLog();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

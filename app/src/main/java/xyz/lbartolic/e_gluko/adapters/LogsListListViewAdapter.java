package xyz.lbartolic.e_gluko.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.utils.DateUtil;
import xyz.lbartolic.e_gluko.utils.UnitsUtil;

/**
 * Created by asus on 16.08.16..
 */
public class LogsListListViewAdapter extends BaseAdapter {
    private Context context;
    private List<HealthLog> healthLogs;
    private ListData listData;
    private HealthLogsList healthLogsList;

    public LogsListListViewAdapter(Context ctx, ListData listData, HealthLogsList healthLogsList) {
        super();
        this.context = ctx;
        this.listData = listData;
        this.healthLogs = listData.getHealthLogs();
        this.healthLogsList = healthLogsList;
    }

    @Override
    public int getCount() {
        return this.healthLogs.size();
    }

    @Override
    public int getViewTypeCount() {
        return (getCount() == 0) ? 1 : getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Object getItem(int i) {
        return healthLogs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        HealthLog currentLog = healthLogs.get(i);
        String logType = currentLog.getLogType().getType();

        if (view == null) {
            int layout = R.layout.list_item_log;
            if (logType.equals("glucose")) layout = R.layout.list_item_log_glucose;
            else if (logType.equals("insulin")) layout = R.layout.list_item_log_insulin;
            else if (logType.equals("physical_activity")) layout = R.layout.list_item_log_physical_activity;
            else if (logType.equals("step")) layout = R.layout.list_item_log_step;
            else if (logType.equals("calorie")) layout = R.layout.list_item_log_calorie;
            else if (logType.equals("sleep")) layout = R.layout.list_item_log_sleep;
            view = View.inflate(context, layout, null);
        }


        TextView tvTime = (TextView) view.findViewById(R.id.tvTime);
        TextView tvValue = (TextView) view.findViewById(R.id.tvValue);
        LinearLayout llInsertDevice = (LinearLayout) view.findViewById(R.id.llInsertDevice);
        LinearLayout llInsertUser = (LinearLayout) view.findViewById(R.id.llInsertUser);
        if (currentLog.getInsertType() == 0) llInsertUser.setVisibility(View.VISIBLE);
        if (currentLog.getInsertType() == 1) llInsertDevice.setVisibility(View.VISIBLE);
        tvTime.setText(DateUtil.formatDateString(currentLog.getLogTs(), DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_TIME_FORMAT));
        tvValue.setText(currentLog.getValueFormatted());


        if (logType.equals("glucose") || logType.equals("insulin")) {
            TextView tvMealRelation = (TextView) view.findViewById(R.id.tvMealRelation);
            int mealRelColor = R.color.undefined_meal_rel;
            if (currentLog.getHealthLogChild().getLabel().getMealRel() == 1) {
                mealRelColor = R.color.before_meal;
            }
            else if (currentLog.getHealthLogChild().getLabel().getMealRel() == 2) {
                mealRelColor = R.color.after_meal;
            }
            tvMealRelation.setText(currentLog.getHealthLogChild().getLabel().getDisplayValue());
            tvMealRelation.setBackgroundColor(ContextCompat.getColor(context, mealRelColor));
            if (logType.equals("glucose")) {}
            if (logType.equals("insulin")) {
                int insulinTypeColor = R.color.basal;
                TextView tvInsulinType = (TextView) view.findViewById(R.id.tvInsulinType);
                if (currentLog.getHealthLogChild().getInsulinType().getKey().equals("bolus")) {
                    insulinTypeColor = R.color.bolus;
                }
                tvInsulinType.setText(currentLog.getHealthLogChild().getInsulinType().getDisplayValue());
                tvInsulinType.setBackgroundColor(ContextCompat.getColor(context, insulinTypeColor));
            }
        }
        else if (logType.equals("physical_activity")) {
            TextView tvCalories = (TextView) view.findViewById(R.id.tvCalories);
            TextView tvSteps = (TextView) view.findViewById(R.id.tvSteps);
            TextView tvHeartRate = (TextView) view.findViewById(R.id.tvHeartRate);
            tvValue.setText(UnitsUtil.formatHmsFromMillis((int)(currentLog.getValue())));
            tvCalories.setText(String.valueOf(currentLog.getHealthLogChild().getCalories()));
            tvSteps.setText(String.valueOf(currentLog.getHealthLogChild().getSteps()));
            tvHeartRate.setText(String.valueOf(currentLog.getHealthLogChild().getAverageHeartRate()));
        }
        else if (logType.equals("step")) {

        }
        else if (logType.equals("calorie")) {

        }
        else if (logType.equals("sleep")) {
            tvValue.setText(UnitsUtil.formatHmsFromMillis((int)(currentLog.getValue())));
            TextView tvAsleep = (TextView) view.findViewById(R.id.tvAsleep);
            TextView tvAwake = (TextView) view.findViewById(R.id.tvAwake);
            TextView tvRestless = (TextView) view.findViewById(R.id.tvRestless);
            TextView tvAwakeCount = (TextView) view.findViewById(R.id.tvAwakeCount);
            TextView tvRestlessCount = (TextView) view.findViewById(R.id.tvRestlessCount);
            tvAsleep.setText(UnitsUtil.formatHmsFromMins((currentLog.getHealthLogChild().getAsleepMins())));
            tvAwake.setText(UnitsUtil.formatHmsFromMins((currentLog.getHealthLogChild().getAwakeMins())));
            tvRestless.setText(UnitsUtil.formatHmsFromMins((currentLog.getHealthLogChild().getRestlessMins())));
            tvAwakeCount.setText(String.valueOf(currentLog.getHealthLogChild().getAwakeCount()));
            tvRestlessCount.setText(String.valueOf(currentLog.getHealthLogChild().getRestlessCount()));
        }

        return view;
    }
}

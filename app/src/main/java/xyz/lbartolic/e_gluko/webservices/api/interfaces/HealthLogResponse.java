package xyz.lbartolic.e_gluko.webservices.api.interfaces;

import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogCreateData;

/**
 * Created by asus on 17.08.16..
 */
public interface HealthLogResponse {
    void onSuccess(HealthLog healthLog);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}

package xyz.lbartolic.e_gluko.models.Statistics.Glucose;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;
import xyz.lbartolic.e_gluko.utils.StatsFormatUtil;
import xyz.lbartolic.e_gluko.models.Statistics.StatsItem;

/**
 * Created by asus on 06.09.17..
 */
public class GlucoseStats {
    private static final String PREFS_KEY = "glucose_stats";

    @SerializedName("total_logs")
    private int totalLogs;
    @SerializedName("logged_days")
    private int loggedDays;
    @SerializedName("avg_logs_per_day")
    private int averageLogsPerDay;
    @SerializedName("high_percentage")
    private float highPercentage;
    @SerializedName("normal_percentage")
    private float normalPercentage;
    @SerializedName("low_percentage")
    private float lowPercentage;
    @SerializedName("max_value")
    private StatsDescriptiveGoalDiff maxValue;
    @SerializedName("min_value")
    private StatsDescriptiveGoalDiff minValue;
    @SerializedName("avg_value")
    private StatsDescriptiveGoalDiff avgValue;
    @SerializedName("estimated_a1c")
    private StatsDescriptiveGoalDiff estimatedA1c;
    @SerializedName("before_after")
    private GlucoseBeforeAfter beforeAfter;
    @SerializedName("goal_avgs")
    private GoalAvgs goalAvgs;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public List<StatsItem> getStatsItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, maxValue, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.max_glucose)),
                new StatsItem(null, minValue, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.min_glucose)),
                new StatsItem(null, avgValue, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_glucose)),
                new StatsItem(null, estimatedA1c, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.estimated_a1c)),
                new StatsItem(highPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.high_perc_glucose)),
                new StatsItem(lowPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.low_perc_glucose)),
                new StatsItem(normalPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.normal_perc_glucose)),
                new StatsItem((float) averageLogsPerDay, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_logs_daily_glucose))
        );
    }

    public int getTotalLogs() {
        return totalLogs;
    }

    public int getLoggedDays() {
        return loggedDays;
    }

    public int getAverageLogsPerDay() {
        return averageLogsPerDay;
    }

    public float getHighPercentage() {
        return highPercentage;
    }

    public float getNormalPercentage() {
        return normalPercentage;
    }

    public float getLowPercentage() {
        return lowPercentage;
    }

    public StatsDescriptiveGoalDiff getMaxValue() {
        return maxValue;
    }

    public StatsDescriptiveGoalDiff getMinValue() {
        return minValue;
    }

    public StatsDescriptiveGoalDiff getAvgValue() {
        return avgValue;
    }

    public StatsDescriptiveGoalDiff getEstimatedA1c() {
        return estimatedA1c;
    }

    public GlucoseBeforeAfter getBeforeAfter() {
        return beforeAfter;
    }

    public GoalAvgs getGoalAvgs() {
        return goalAvgs;
    }
}

package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by asus on 03.09.17..
 */
public class HealthLogType {
    private static final String PREFS_KEY = "health_log_type";
    public static final String TYPE_GLUCOSE = "glucose";
    public static final String TYPE_INSULIN = "insulin";
    public static final String TYPE_PHYSICAL_ACTIVITY = "physical_activity";
    public static final String TYPE_STEP = "step";
    public static final String TYPE_CALORIE = "calorie";
    public static final String TYPE_SLEEP = "sleep";
    public static final String FORMAT_TYPE_TIME = "time";
    public static final String FORMAT_TYPE_DEFAULT = "default";

    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("type_name")
    private String typeName;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public static List<String> getAllTypes() {
        return new ArrayList<>(Arrays.asList(
           TYPE_GLUCOSE, TYPE_INSULIN, TYPE_PHYSICAL_ACTIVITY, TYPE_SLEEP, TYPE_STEP, TYPE_CALORIE
        ));
    }
}

package xyz.lbartolic.e_gluko.models.HealthLogsList;

import com.google.gson.annotations.SerializedName;

import xyz.lbartolic.e_gluko.models.HealthGoal;
import xyz.lbartolic.e_gluko.models.HealthLog;

/**
 * Created by asus on 03.09.17..
 */
public class StatsDescriptiveGoalDiff {
    private static final String PREFS_KEY = "stats_descriptive_goal_diff";

    @SerializedName("log")
    private HealthLog log;
    @SerializedName("value")
    private Float value;
    @SerializedName("desc")
    private String desc;
    @SerializedName("deviation_percentage")
    private Float deviationPercentage;

    public StatsDescriptiveGoalDiff(HealthLog log, Float value, String desc, Float deviationPercentage) {
        this.log = log;
        this.value = value;
        this.desc = desc;
        this.deviationPercentage = deviationPercentage;
    }

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public HealthLog getLog() {
        return log;
    }

    public Float getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public Float getDeviationPercentage() {
        return deviationPercentage;
    }

    public String test() {
        return "TEST";
    }

    public static StatsDescriptiveGoalDiff getGlucoseLogDescGoalDiff(HealthLog log, HealthGoal healthGoal) {
        float minBefore = healthGoal.getMinBgBeforeMeal();
        float maxBefore = healthGoal.getMaxBgBeforeMeal();
        float maxAfter = healthGoal.getMaxBgAfterMeal();
        int mealRel = log.getHealthLogChild().getLabel().getMealRel();
        float value = log.getValue();
        float maxValue = maxAfter;
        float minValue = minBefore;
        if (mealRel == 1 || mealRel == 2) {
            if (mealRel == 1) {
                maxValue = maxBefore;
                minValue = minBefore;
            }
            if (mealRel == 2) {
                maxValue = maxAfter;
                minValue = minBefore;
            }
        }

        String desc = "normal";
        Float deviationPerc = null;
        if (value < minValue) {
            desc = "low";
            deviationPerc = ((value/minBefore)*100) - 100;
        }
        if (value > maxValue) {
            desc = "high";
            deviationPerc = ((value/maxAfter)*100) - 100;
        }

        return new StatsDescriptiveGoalDiff(log, null, desc, deviationPerc);
    }
}

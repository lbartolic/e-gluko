package xyz.lbartolic.e_gluko.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by asus on 17.08.16..
 */
public class DateUtil {
    public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DB_TIME_FORMAT = "HH:mm:ss";
    public static final String DB_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String APP_DATE_TIME_FORMAT = "dd.MM.yyyy. HH:mm:ss";
    public static final String APP_DATE_TIME_FORMAT_LONG = "EEEE, dd.MM.yyyy. HH:mm:ss";
    public static final String APP_DATE_FORMAT_DATE = "dd.MM.yyyy.";
    public static final String APP_DATE_FORMAT_DATE_DM = "dd.MM.";
    public static final String APP_DATE_FORMAT_LONG = "EEEE, dd.MM.yyyy.";
    public static final String APP_DATE_FORMAT_LONGER = "EEEE, MMMM d, yyyy";
    public static final String APP_DATE_FORMAT_SHORT = "EEE, MMM d";
    public static final String APP_TIME_FORMAT = "HH:mm";
    public static final String APP_DATE_FORMAT_DAY = "EEE";
    private static final Locale locale = new Locale("hr", "HR");

    public static Date convertStringToDate(String dateString, String inputDateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(inputDateFormat, locale);
        try {
            Date inputDate = sdf.parse(dateString);
            return inputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDateToString(Date date, String outputDateFormat) {
        SimpleDateFormat sdfOut = new SimpleDateFormat(outputDateFormat, locale);
        return sdfOut.format(date);
    }

    public static String formatDateString(String inputDateString, String inputDateFormat, String outputDateFormat) {
        Date inputDate = DateUtil.convertStringToDate(inputDateString, inputDateFormat);
        String outputDate = DateUtil.convertDateToString(inputDate, outputDateFormat);
        return outputDate;
    }

    public static String getCurrentDate() {
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(DB_DATE_FORMAT);
        return df.format(currentTime.getTime());
    }

    public static int diffInDays(Date dateFrom, Date dateTo) {
        long diff = dateFrom.getTime() - dateTo.getTime();
        Double days = Math.ceil((diff / (1000*60*60*24)));
        return Math.abs(days.intValue());
    }
}

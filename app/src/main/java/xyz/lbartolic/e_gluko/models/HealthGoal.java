package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 03.09.17..
 */
public class HealthGoal {
    private static final String PREFS_KEY = "health_goal";

    @SerializedName("id")
    private int id;
    @SerializedName("max_a1c")
    private float maxA1c;
    @SerializedName("max_sistolic_bp")
    private int maxSistolicBp;
    @SerializedName("max_diastolic_bp")
    private int maxDiastolicBp;
    @SerializedName("max_total_cholesterol")
    private float maxTotalCholesterol;
    @SerializedName("min_hdl_cholesterol")
    private float minHdlCholesterol;
    @SerializedName("max_ldl_cholesterol")
    private float maxLdlCholesterol;
    @SerializedName("min_bg_before_meal")
    private float minBgBeforeMeal;
    @SerializedName("max_bg_before_meal")
    private float maxBgBeforeMeal;
    @SerializedName("max_bg_after_meal")
    private float maxBgAfterMeal;
    @SerializedName("weight")
    private float weight;
    @SerializedName("min_daily_steps")
    private int minDailySteps;
    @SerializedName("min_daily_excercise")
    private int minDailyExcercise;
    @SerializedName("goal_from_ts")
    private String goalFromTs;
    @SerializedName("daily_basal_total")
    private int dailyBasalTotal;
    @SerializedName("daily_bolus_total")
    private int dailyBolusTotal;
    @SerializedName("tdd")
    private int tdd;
    @SerializedName("user_id")
    private int userId;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public float getMaxA1c() {
        return maxA1c;
    }

    public int getMaxSistolicBp() {
        return maxSistolicBp;
    }

    public int getMaxDiastolicBp() {
        return maxDiastolicBp;
    }

    public float getMaxTotalCholesterol() {
        return maxTotalCholesterol;
    }

    public float getMinHdlCholesterol() {
        return minHdlCholesterol;
    }

    public float getMaxLdlCholesterol() {
        return maxLdlCholesterol;
    }

    public float getMinBgBeforeMeal() {
        return minBgBeforeMeal;
    }

    public float getMaxBgBeforeMeal() {
        return maxBgBeforeMeal;
    }

    public float getMaxBgAfterMeal() {
        return maxBgAfterMeal;
    }

    public float getWeight() {
        return weight;
    }

    public int getMinDailySteps() {
        return minDailySteps;
    }

    public int getMinDailyExcercise() {
        return minDailyExcercise;
    }

    public String getGoalFromTs() {
        return goalFromTs;
    }

    public int getDailyBasalTotal() {
        return dailyBasalTotal;
    }

    public int getDailyBolusTotal() {
        return dailyBolusTotal;
    }

    public int getTdd() {
        return tdd;
    }

    public int getUserId() {
        return userId;
    }
}

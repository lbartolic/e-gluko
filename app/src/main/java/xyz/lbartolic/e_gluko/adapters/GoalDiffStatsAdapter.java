package xyz.lbartolic.e_gluko.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.activities.LogsActivity;
import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;
import xyz.lbartolic.e_gluko.utils.StatsFormatUtil;
import xyz.lbartolic.e_gluko.models.Statistics.StatsItem;

/**
 * Created by asus on 08.09.17..
 */
public class GoalDiffStatsAdapter extends BaseAdapter {
    private Context context;
    private List<StatsItem> statsItems;

    public GoalDiffStatsAdapter(Context context, List<StatsItem> statsItems) {
        this.context = context;
        this.statsItems = statsItems;
    }

    @Override
    public int getCount() {
        return statsItems.size();
    }

    @Override
    public Object getItem(int i) {
        return statsItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        StatsItem currentStatsItem = statsItems.get(i);
        LinearLayout statsItem;
        if (view == null) {
            statsItem = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.item_period_stats, viewGroup, false);
        } else {
            statsItem = (LinearLayout) view;
        }

        TextView tvValue = (TextView) statsItem.findViewById(R.id.tvValue);
        ImageView ivIndicator = (ImageView) statsItem.findViewById(R.id.ivIndicator);
        TextView tvTitle = (TextView) statsItem.findViewById(R.id.tvTitle);
        TextView tvDeviationPerc = (TextView) statsItem.findViewById(R.id.tvDeviationPerc);

        String valueStr = "-";
        Float value = null;
        Float deviationPerc = null;
        Integer indicator = null;
        Integer indicatorColor = null;

        StatsDescriptiveGoalDiff descriptiveGoalDiff = currentStatsItem.getStatsDescriptiveGoalDiff();
        if (descriptiveGoalDiff != null) {
            if (descriptiveGoalDiff.getLog() != null) {
                value = descriptiveGoalDiff.getLog().getValue();
            } else if (descriptiveGoalDiff.getValue() != null) {
                value = descriptiveGoalDiff.getValue();
            }
            if (descriptiveGoalDiff.getDeviationPercentage() != null) {
                deviationPerc = descriptiveGoalDiff.getDeviationPercentage();
            }

            Integer[] viewValues = LogsActivity.getGoalDiffViewValues(value, descriptiveGoalDiff.getDesc(), currentStatsItem.getType());
            indicator = viewValues[0];
            indicatorColor = viewValues[1];
        }
        else {
            value = currentStatsItem.getValue();
        }

        if (value != null) {
            valueStr = StatsFormatUtil.formatStatsValue(value, currentStatsItem.getFormatType());
        }

        tvValue.setText(valueStr);
        tvTitle.setText(currentStatsItem.getTitle());
        tvDeviationPerc.setText(currentStatsItem.getTitle());

        if (deviationPerc != null && deviationPerc != 0) {
            tvDeviationPerc.setText(deviationPerc + "%");
            tvDeviationPerc.setVisibility(View.VISIBLE);
        }
        if (indicator != null) {
            ivIndicator.setImageResource(indicator);
            ivIndicator.setColorFilter(context.getResources().getColor(indicatorColor));
        } else {
            ivIndicator.setVisibility(View.GONE);
        }

        return statsItem;
    }
}

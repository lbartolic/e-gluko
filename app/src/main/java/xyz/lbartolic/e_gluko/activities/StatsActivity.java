package xyz.lbartolic.e_gluko.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.adapters.TotalStatsPagerAdapter;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;
import xyz.lbartolic.e_gluko.models.User;
import xyz.lbartolic.e_gluko.utils.DateUtil;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.TotalStatsResponse;
import xyz.lbartolic.e_gluko.webservices.api.repositories.HealthLogRepository;

public class StatsActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private Toolbar toolbar;
    private EditText etDate;
    private Button btnLogsPrev;
    private Button btnLogsNext;
    private TabLayout tlType;
    private ViewPager typePager;
    private PagerAdapter pagerAdapter;
    private ProgressBar pbStats;
    private String currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        currentDate = DateUtil.getCurrentDate();
        initUI();
        setStats();
    }

    private void setStats() {
        showLoading();
        HealthLogRepository.getTotalStats(User.getAuthUserApiToken(this), "2017-08-01", new TotalStatsResponse() {
            @Override
            public void onSuccess(final TotalStats totalStats) {
                pagerAdapter = new TotalStatsPagerAdapter(getSupportFragmentManager(), getApplicationContext(), totalStats);
                typePager.setAdapter(pagerAdapter);
                tlType.setupWithViewPager(typePager);
                Log.d("STATS_TEST", totalStats.getStatsByType().getGlucose().getAvgValue().getValue()+"");

                /*String dateFrom = DateUtil.formatDateString(healthLogsList.getDateFrom(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_DATE);
                String dateTo = DateUtil.formatDateString(healthLogsList.getDateTo(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_DATE);
                etDate.setText(dateFrom + " - " + dateTo);*/

                hideLoading();
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("ERR", throwable.toString());
            }
        });
    }

    private void showLoading() {
        typePager.setVisibility(View.GONE);
        tlType.setVisibility(View.GONE);
        pbStats.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        pbStats.setVisibility(View.GONE);
        tlType.setVisibility(View.VISIBLE);
        typePager.setVisibility(View.VISIBLE);
    }

    private void initUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.activity_stats_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pbStats = (ProgressBar) findViewById(R.id.pbStats);
        btnLogsPrev = (Button) findViewById(R.id.btnLogsPrev);
        btnLogsNext = (Button) findViewById(R.id.btnLogsNext);
        etDate = (EditText) findViewById(R.id.etDate);
        typePager = (ViewPager) findViewById(R.id.viewPager);
        tlType = (TabLayout) findViewById(R.id.tlType);
        typePager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlType));
        tlType.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                typePager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        btnLogsPrev.setOnClickListener(this);
        btnLogsNext.setOnClickListener(this);
        etDate.setOnClickListener(this);
        etDate.setFocusable(false);
        etDate.setClickable(true);
    }



    @Override
    public void onClick(View view) {
        Date currentDate = DateUtil.convertStringToDate(this.currentDate, DateUtil.DB_DATE_FORMAT);
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                StatsActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        switch (view.getId()) {
            case R.id.etDate:
                dpd.show(getFragmentManager(), "datepicker_logs_list");
                break;
            case R.id.btnLogsPrev:
                this.currentDate = DateUtil.convertDateToString(new Date(currentDate.getTime() - 7 * 24 * 3600 * 1000), DateUtil.DB_DATE_FORMAT).toString();
                setStats();
                break;
            case R.id.btnLogsNext:
                this.currentDate = DateUtil.convertDateToString(new Date(currentDate.getTime() + 7 * 24 * 3600 * 1000), DateUtil.DB_DATE_FORMAT).toString();
                setStats();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String dateString = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        String dateStringFormat = DateUtil.formatDateString(dateString, "yyyy-MM-dd", DateUtil.APP_DATE_FORMAT_LONG);
        etDate.setText(dateStringFormat);
        currentDate = dateString;
        setStats();
    }
}

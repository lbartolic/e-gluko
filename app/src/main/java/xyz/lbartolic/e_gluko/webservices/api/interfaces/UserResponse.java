package xyz.lbartolic.e_gluko.webservices.api.interfaces;


import xyz.lbartolic.e_gluko.models.User;

/**
 * Created by asus on 14.08.16..
 */
public interface UserResponse {
    void onSuccess(User user);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}

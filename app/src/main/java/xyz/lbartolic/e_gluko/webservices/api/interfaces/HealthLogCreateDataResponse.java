package xyz.lbartolic.e_gluko.webservices.api.interfaces;

import xyz.lbartolic.e_gluko.models.HealthLogCreateData;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;

/**
 * Created by asus on 17.08.16..
 */
public interface HealthLogCreateDataResponse {
    void onSuccess(HealthLogCreateData createData);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}

package xyz.lbartolic.e_gluko.models;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import xyz.lbartolic.e_gluko.utils.JsonParseUtil;
import xyz.lbartolic.e_gluko.utils.PreferencesUtil;

/**
 * Created by asus on 02.09.17..
 */
public class User {
    private static final String PREFS_KEY = "current_user";

    @SerializedName("id")
    private int id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    private String email;
    @SerializedName("api_token")
    private String apiToken;
    @SerializedName("created_at")
    private String createdAt;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public static void setUserPrefs(Context context, User user) {
        String userJson = JsonParseUtil.convertObjectToJson(user);
        PreferencesUtil.getSharedPreferences(context).edit()
                .putString(PREFS_KEY, userJson).apply();
    }

    public static void removeUserPrefs(Context context) {
        PreferencesUtil.getSharedPreferences(context).edit()
                .remove(PREFS_KEY).apply();
    }

    public static User getAuthUser(Context context) {
        User user = new Gson().fromJson(PreferencesUtil.getSharedPreferences(context).getString(User.getPrefsKey(), null), User.class);
        return user;
    }

    public static String getAuthUserApiToken(Context context) {
        User user = User.getAuthUser(context);
        return user.getApiToken();
    }

    public static int getAuthUserId(Context context) {
        User user = User.getAuthUser(context);
        return user.getId();
    }

    public static String getAuthUserFullName(Context context) {
        User user = User.getAuthUser(context);
        return user.getFullName();
    }

    public static String getAuthUserEmail(Context context) {
        User user = User.getAuthUser(context);
        return user.getEmail();
    }

    public static boolean isLoggedIn(Context context) {
        return PreferencesUtil.getSharedPreferences(context).contains(PREFS_KEY);
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getApiToken() {
        return apiToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}


package xyz.lbartolic.e_gluko.models.Statistics;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import xyz.lbartolic.e_gluko.models.Statistics.Glucose.GlucoseStats;
import xyz.lbartolic.e_gluko.models.Statistics.Insulin.InsulinStats;

/**
 * Created by asus on 08.09.17..
 */
public class StatsByType {
    @SerializedName("glucose")
    GlucoseStats glucose;
    @SerializedName("insulin")
    InsulinStats insulin;
    @SerializedName("physical_activity")
    JsonObject physicalActivity;
    @SerializedName("step")
    JsonObject step;
    @SerializedName("calorie")
    JsonObject calorie;
    @SerializedName("sleep")
    JsonObject sleep;

    public GlucoseStats getGlucose() {
        return glucose;
    }

    public InsulinStats getInsulin() {
        return insulin;
    }

    public JsonObject getPhysicalActivity() {
        return physicalActivity;
    }

    public JsonObject getStep() {
        return step;
    }

    public JsonObject getCalorie() {
        return calorie;
    }

    public JsonObject getSleep() {
        return sleep;
    }
}

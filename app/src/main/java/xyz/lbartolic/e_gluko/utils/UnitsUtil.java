package xyz.lbartolic.e_gluko.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by asus on 03.09.17..
 */
public class UnitsUtil {
    public static String formatHmsFromMillis(int ms) {
        int seconds = (int)(ms/1000)%60 ;
        int minutes = (int)((ms/(1000*60))%60);
        int hours   = (int)((ms/(1000*60*60)));
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String formatHmsFromMins(int mins) {
        return formatHmsFromMillis(mins*60*100);
    }

    public static String getValueFormatted(float value) {
        if (value == (long)value)
            return String.format("%d", (long)value);
        else
            return String.format("%s", value);
    }
}

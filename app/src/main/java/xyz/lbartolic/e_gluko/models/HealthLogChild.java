package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

import xyz.lbartolic.e_gluko.models.HealthLogLabel;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.InsulinType;

/**
 * Created by asus on 03.09.17..
 */
public class HealthLogChild {
    private static final String PREFS_KEY = "health_log_child";

    @SerializedName("id")
    private int id;
    @SerializedName("insulin_type")
    private InsulinType insulinType;
    @SerializedName("label")
    private HealthLogLabel label;
    @SerializedName("asleep_mins")
    private int asleepMins;
    @SerializedName("awake_mins")
    private int awakeMins;
    @SerializedName("restless_mins")
    private int restlessMins;
    @SerializedName("sleep_quality")
    private int sleepQuality;
    @SerializedName("awake_count")
    private int awakeCount;
    @SerializedName("restless_count")
    private int restlessCount;
    @SerializedName("calories")
    private int calories;
    @SerializedName("steps")
    private int steps;
    @SerializedName("average_heart_rate")
    private int averageHeartRate;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public InsulinType getInsulinType() {
        return insulinType;
    }

    public HealthLogLabel getLabel() {
        return label;
    }

    public int getAsleepMins() {
        return asleepMins;
    }

    public int getAwakeMins() {
        return awakeMins;
    }

    public int getRestlessMins() {
        return restlessMins;
    }

    public int getSleepQuality() {
        return sleepQuality;
    }

    public int getAwakeCount() {
        return awakeCount;
    }

    public int getRestlessCount() {
        return restlessCount;
    }

    public int getCalories() {
        return calories;
    }

    public int getSteps() {
        return steps;
    }

    public int getAverageHeartRate() {
        return averageHeartRate;
    }
}

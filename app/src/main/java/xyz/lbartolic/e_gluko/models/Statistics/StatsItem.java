package xyz.lbartolic.e_gluko.models.Statistics;

import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;

/**
 * Created by asus on 08.09.17..
 */
public class StatsItem {
    private Float value;
    private StatsDescriptiveGoalDiff statsDescriptiveGoalDiff;
    private String type;
    private String formatType;
    private String title;

    public StatsItem(Float value, StatsDescriptiveGoalDiff statsDescriptiveGoalDiff, String type, String formatType, String title) {
        this.value = value;
        this.statsDescriptiveGoalDiff = statsDescriptiveGoalDiff;
        this.type = type;
        this.formatType = formatType;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Float getValue() {
        return value;
    }

    public StatsDescriptiveGoalDiff getStatsDescriptiveGoalDiff() {
        return statsDescriptiveGoalDiff;
    }

    public String getType() {
        return type;
    }

    public String getFormatType() {
        return formatType;
    }
}

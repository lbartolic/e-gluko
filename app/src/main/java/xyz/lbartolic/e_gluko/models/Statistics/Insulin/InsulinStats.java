package xyz.lbartolic.e_gluko.models.Statistics.Insulin;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;
import xyz.lbartolic.e_gluko.models.Statistics.StatsItem;
import xyz.lbartolic.e_gluko.utils.StatsFormatUtil;

/**
 * Created by asus on 06.09.17..
 */
public class InsulinStats {


    @SerializedName("logged_days")
    private int loggedDays;
    @SerializedName("total_logs")
    private int totalLogs;
    @SerializedName("total_logs_basal")
    private int totalLogsBasal;
    @SerializedName("total_logs_bolus")
    private int totalLogsBolus;
    @SerializedName("total_units_basal")
    private int totalUnitsBasal;
    @SerializedName("total_units_bolus")
    private int totalUnitsBolus;
    @SerializedName("avg_units_after_meal")
    private float avgUnitsAfterMeal;
    @SerializedName("avg_units_basal")
    private float avgUnitsBasal;
    @SerializedName("avg_units_before_bed")
    private float avgUnitsBeforeBed;
    @SerializedName("avg_units_before_meal")
    private float avgUnitsBeforeMeal;
    @SerializedName("avg_units_bolus")
    private float avgUnitsBolus;
    @SerializedName("basal_after_meal")
    private InsulinNumerics basalAfterMeal;
    @SerializedName("basal_before_bed")
    private InsulinNumerics basalBeforeBed;
    @SerializedName("basal_before_meal")
    private InsulinNumerics basalBeforeMeal;
    @SerializedName("bolus_after_meal")
    private InsulinNumerics bolusAfterMeal;
    @SerializedName("bolus_before_bed")
    private InsulinNumerics bolusBeforeBed;
    @SerializedName("bolus_before_meal")
    private InsulinNumerics bolusBeforeMeal;
    @SerializedName("daily_avg_units_basal")
    private StatsDescriptiveGoalDiff dailyAvgUnitsBasal;
    @SerializedName("daily_avg_units_bolus")
    private StatsDescriptiveGoalDiff dailyAvgUnitsBolus;
    @SerializedName("daily_avg_units_total")
    private StatsDescriptiveGoalDiff dailyAvgUnitsTotal;

    public List<StatsItem> getTotalStatsItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, dailyAvgUnitsTotal, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_daily_insulin)),
                new StatsItem(avgUnitsBeforeMeal, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_meal_insulin)),
                new StatsItem(avgUnitsAfterMeal, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_after_meal_insulin)),
                new StatsItem(avgUnitsBeforeBed, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_bed_insulin)),
                new StatsItem((float)getTotalUnits(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.total_units_insulin))
        );
    }

    public List<StatsItem> getBasalStatsItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, dailyAvgUnitsBasal, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_daily_basal)),
                new StatsItem(basalBeforeMeal.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_meal_basal)),
                new StatsItem(basalAfterMeal.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_after_meal_basal)),
                new StatsItem(basalBeforeBed.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_bed_basal)),
                new StatsItem((float)totalLogsBasal, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.total_logs_basal)),
                new StatsItem((float)totalUnitsBasal, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.total_units_basal))
        );
    }

    public List<StatsItem> getBolusStatsItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, dailyAvgUnitsBolus, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_daily_bolus)),
                new StatsItem(bolusBeforeMeal.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_meal_bolus)),
                new StatsItem(bolusAfterMeal.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_after_meal_bolus)),
                new StatsItem(bolusBeforeBed.getAvg(), null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_before_bed_bolus)),
                new StatsItem((float)totalLogsBolus, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.total_logs_bolus)),
                new StatsItem((float)totalUnitsBolus, null, HealthLogType.TYPE_INSULIN, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.total_units_bolus))
        );
    }

    public int getTotalUnits() {
        return totalUnitsBasal + totalUnitsBolus;
    }

    public int getLoggedDays() {
        return loggedDays;
    }

    public int getTotalLogs() {
        return totalLogs;
    }

    public int getTotalLogsBasal() {
        return totalLogsBasal;
    }

    public int getTotalLogsBolus() {
        return totalLogsBolus;
    }

    public int getTotalUnitsBasal() {
        return totalUnitsBasal;
    }

    public int getTotalUnitsBolus() {
        return totalUnitsBolus;
    }

    public float getAvgUnitsAfterMeal() {
        return avgUnitsAfterMeal;
    }

    public float getAvgUnitsBasal() {
        return avgUnitsBasal;
    }

    public float getAvgUnitsBeforeBed() {
        return avgUnitsBeforeBed;
    }

    public float getAvgUnitsBeforeMeal() {
        return avgUnitsBeforeMeal;
    }

    public float getAvgUnitsBolus() {
        return avgUnitsBolus;
    }

    public InsulinNumerics getBasalAfterMeal() {
        return basalAfterMeal;
    }

    public InsulinNumerics getBasalBeforeBed() {
        return basalBeforeBed;
    }

    public InsulinNumerics getBasalBeforeMeal() {
        return basalBeforeMeal;
    }

    public InsulinNumerics getBolusAfterMeal() {
        return bolusAfterMeal;
    }

    public InsulinNumerics getBolusBeforeBed() {
        return bolusBeforeBed;
    }

    public InsulinNumerics getBolusBeforeMeal() {
        return bolusBeforeMeal;
    }

    public StatsDescriptiveGoalDiff getDailyAvgUnitsBasal() {
        return dailyAvgUnitsBasal;
    }

    public StatsDescriptiveGoalDiff getDailyAvgUnitsBolus() {
        return dailyAvgUnitsBolus;
    }

    public StatsDescriptiveGoalDiff getDailyAvgUnitsTotal() {
        return dailyAvgUnitsTotal;
    }
}

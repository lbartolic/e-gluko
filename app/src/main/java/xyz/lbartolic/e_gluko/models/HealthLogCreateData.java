package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import xyz.lbartolic.e_gluko.models.HealthLogLabel;
import xyz.lbartolic.e_gluko.models.InsulinType;

/**
 * Created by asus on 13.09.17..
 */
public class HealthLogCreateData {
    @SerializedName("labels")
    private List<HealthLogLabel> labels;
    @SerializedName("insulin_types")
    private List<InsulinType> insulinTypes;

    public List<HealthLogLabel> getLabels() {
        return labels;
    }

    public List<InsulinType> getInsulinTypes() {
        return insulinTypes;
    }
}

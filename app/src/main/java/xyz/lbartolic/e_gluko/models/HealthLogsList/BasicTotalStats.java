package xyz.lbartolic.e_gluko.models.HealthLogsList;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import xyz.lbartolic.e_gluko.models.HealthLogType;

/**
 * Created by asus on 03.09.17..
 */
public class BasicTotalStats {
    private static final String PREFS_KEY = "basic_total_stats";
    public static final String[] glucoseKeys = {"glucose_avg", "glucose_max", "glucose_min"};
    public static final String[] insulinKeys = {"insulin_basal_daily_avg", "insulin_bolus_daily_avg"};
    public static final String[] activityKeys = {"activity_total", "activity_daily_avg", "activity_heart_rate_avg"};
    public static final String[] calorieKeys = {"calorie_daily_avg"};
    public static final String[] stepKeys = {"step_daily_avg"};
    public static final String[] sleepKeys = {"sleep_total", "sleep_avg", "sleep_avg_quality"};

    public static final String[] timeFormatKeys = {"activity_total", "activity_daily_avg", "sleep_total", "sleep_avg"};

    @SerializedName("glucose_avg")
    private StatsDescriptiveGoalDiff glucoseAvg;
    @SerializedName("glucose_max")
    private StatsDescriptiveGoalDiff glucoseMax;
    @SerializedName("glucose_min")
    private StatsDescriptiveGoalDiff glucoseMin;
    @SerializedName("insulin_basal_daily_avg")
    private StatsDescriptiveGoalDiff insulinBasalDailyAvg;
    @SerializedName("insulin_bolus_daily_avg")
    private StatsDescriptiveGoalDiff insulinBolusDailyAvg;
    @SerializedName("activity_total")
    private StatsDescriptiveGoalDiff activityTotal;
    @SerializedName("activity_daily_avg")
    private float activityDailyAvg;
    @SerializedName("activity_heart_rate_avg")
    private float activityHeartRateAvg;
    @SerializedName("calorie_daily_avg")
    private StatsDescriptiveGoalDiff calorieDailyAvg;
    @SerializedName("step_daily_avg")
    private StatsDescriptiveGoalDiff stepDailyAvg;
    @SerializedName("sleep_total")
    private StatsDescriptiveGoalDiff sleepTotal;
    @SerializedName("sleep_avg")
    private float sleepAvg;
    @SerializedName("sleep_avg_quality")
    private float sleepAvgQuality;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public static ArrayList<String[]> getAllKeys() {
        return new ArrayList<>(Arrays.asList(
                glucoseKeys, insulinKeys, activityKeys, calorieKeys, stepKeys, sleepKeys
        ));
    }

    public static ArrayList<String[]> getKeyWithout(String... excludeKeys) {
        ArrayList<String[]> keysArr = getAllKeys();
        for(int i = 0; i < keysArr.size(); i++) {
            String[] keys = keysArr.get(i);
            List<String> list = new ArrayList<>(Arrays.asList(keys));
            for(int j = 0; j < keys.length; j++) {
                for(String excludeKey : excludeKeys) {
                    String key = keysArr.get(i)[j];
                    if (key != null) {
                        if (key.equals(excludeKey)) {
                            list.remove(excludeKey);
                        }
                    }
                }
            }
            keysArr.set(i, list.toArray(new String[0]));
        }
        return keysArr;
    }

    public static String getStatsTypeForKey(String key) {
        if (Arrays.asList(glucoseKeys).contains(key)) return HealthLogType.TYPE_GLUCOSE;
        if (Arrays.asList(insulinKeys).contains(key)) return HealthLogType.TYPE_INSULIN;
        if (Arrays.asList(activityKeys).contains(key)) return HealthLogType.TYPE_PHYSICAL_ACTIVITY;
        if (Arrays.asList(calorieKeys).contains(key)) return HealthLogType.TYPE_CALORIE;
        if (Arrays.asList(stepKeys).contains(key)) return HealthLogType.TYPE_STEP;
        if (Arrays.asList(sleepKeys).contains(key)) return HealthLogType.TYPE_SLEEP;
        return null;
    }

    public static String getFormatTypeForKey(String key) {
        if (Arrays.asList(timeFormatKeys).contains(key)) return HealthLogType.FORMAT_TYPE_TIME;
        return HealthLogType.FORMAT_TYPE_DEFAULT;
    }

    public StatsDescriptiveGoalDiff getGlucoseAvg() {
        return glucoseAvg;
    }

    public StatsDescriptiveGoalDiff getGlucoseMax() {
        return glucoseMax;
    }

    public StatsDescriptiveGoalDiff getGlucoseMin() {
        return glucoseMin;
    }

    public StatsDescriptiveGoalDiff getInsulinBasalDailyAvg() {
        return insulinBasalDailyAvg;
    }

    public StatsDescriptiveGoalDiff getInsulinBolusDailyAvg() {
        return insulinBolusDailyAvg;
    }

    public StatsDescriptiveGoalDiff getActivityTotal() {
        return activityTotal;
    }

    public float getActivityDailyAvg() {
        return activityDailyAvg;
    }

    public float getActivityHeartRateAvg() {
        return activityHeartRateAvg;
    }

    public StatsDescriptiveGoalDiff getCalorieDailyAvg() {
        return calorieDailyAvg;
    }

    public StatsDescriptiveGoalDiff getStepDailyAvg() {
        return stepDailyAvg;
    }

    public StatsDescriptiveGoalDiff getSleepTotal() {
        return sleepTotal;
    }

    public float getSleepAvg() {
        return sleepAvg;
    }

    public float getSleepAvgQuality() {
        return sleepAvgQuality;
    }
}

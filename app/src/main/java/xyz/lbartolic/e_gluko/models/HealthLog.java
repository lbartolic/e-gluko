package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

import xyz.lbartolic.e_gluko.utils.UnitsUtil;

/**
 * Created by asus on 03.09.17..
 */
public class HealthLog {
    private static final String PREFS_KEY = "health_log";

    @SerializedName("id")
    private int id;
    @SerializedName("loggable_id")
    private int loggableId;
    @SerializedName("value")
    private float value;
    @SerializedName("log_ts")
    private String logTs;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("log_type")
    private HealthLogType logType;
    @SerializedName("insert_type")
    private int insertType;
    @SerializedName("loggable")
    private HealthLogChild healthLogChild;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public int getLoggableId() {
        return loggableId;
    }

    public float getValue() {
        return value;
    }

    public String getValueFormatted() {
        return UnitsUtil.getValueFormatted(value);
    }

    public String getLogTs() {
        return logTs;
    }

    public int getUserId() {
        return userId;
    }

    public HealthLogType getLogType() {
        return logType;
    }

    public int getInsertType() {
        return insertType;
    }

    public HealthLogChild getHealthLogChild() {
        return healthLogChild;
    }
}

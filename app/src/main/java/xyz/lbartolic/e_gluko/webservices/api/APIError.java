package xyz.lbartolic.e_gluko.webservices.api;

/**
 * Created by asus on 29.08.16..
 */
public class APIError {

    private int code;
    private String message;

    public APIError() {
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }
}
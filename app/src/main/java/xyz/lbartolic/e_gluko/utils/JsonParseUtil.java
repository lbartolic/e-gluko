package xyz.lbartolic.e_gluko.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by asus on 02.09.17..
 */
public class JsonParseUtil {
    private static Gson gson = new Gson();

    public static String convertObjectToJson(Object object) {
        return gson.toJson(object);
    }

    public static Map<String, Object> mapObject(Object object) {
        return gson.fromJson(convertObjectToJson(object), new TypeToken<Map<String, Object>>(){}.getType());
    }

    public static boolean isSet(JsonElement element) {
        if (element != null && !element.isJsonNull()) return true;
        return false;
    }
}

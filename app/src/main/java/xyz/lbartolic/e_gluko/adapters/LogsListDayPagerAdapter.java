package xyz.lbartolic.e_gluko.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import xyz.lbartolic.e_gluko.fragments.LogsListDayFragment;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.utils.DateUtil;
import xyz.lbartolic.e_gluko.utils.JsonParseUtil;

/**
 * Created by asus on 15.08.16..
 */
public class LogsListDayPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private HealthLogsList healthLogsList;
    private List<ListData> listData;

    public LogsListDayPagerAdapter(FragmentManager fm, Context ctx, HealthLogsList healthLogsList) {
        super(fm);
        this.context = ctx;
        this.healthLogsList = healthLogsList;
        this.listData = healthLogsList.getListData();
    }

    @Override
    public Fragment getItem(int position) {
        LogsListDayFragment lldf = new LogsListDayFragment();
        Bundle bundle = new Bundle();
        bundle.putString("LOGS_LIST_DAY", JsonParseUtil.convertObjectToJson(listData.get(position)));
        bundle.putInt("LOGS_LIST_DAY_POS", position);
        bundle.putString("LOGS_LIST", JsonParseUtil.convertObjectToJson(healthLogsList));
        lldf.setArguments(bundle);
        return lldf;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DateUtil.formatDateString(listData.get(position).getDate(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_DAY).toUpperCase();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

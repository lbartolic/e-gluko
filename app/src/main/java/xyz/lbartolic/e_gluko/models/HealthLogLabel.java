package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by asus on 03.09.17..
 */
public class HealthLogLabel {
    private static final String PREFS_KEY = "health_log_label";


    @SerializedName("id")
    private int id;
    @SerializedName("key")
    private String key;
    @SerializedName("display_value")
    private String displayValue;
    @SerializedName("meal_rel")
    private int mealRel;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public int getMealRel() {
        return mealRel;
    }
}

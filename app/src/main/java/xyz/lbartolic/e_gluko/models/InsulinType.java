package xyz.lbartolic.e_gluko.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 03.09.17..
 */
public class InsulinType {
    private static final String PREFS_KEY = "insulin_type";

    @SerializedName("id")
    private int id;
    @SerializedName("key")
    private String key;
    @SerializedName("display_value")
    private String displayValue;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}

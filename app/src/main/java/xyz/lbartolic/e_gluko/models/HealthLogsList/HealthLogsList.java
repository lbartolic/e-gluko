package xyz.lbartolic.e_gluko.models.HealthLogsList;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import xyz.lbartolic.e_gluko.models.HealthGoal;
import xyz.lbartolic.e_gluko.models.HealthLogType;

/**
 * Created by asus on 16.08.16..
 */
public class HealthLogsList {
    private static final String PREFS_KEY = "health_logs_list";

    @SerializedName("date_from")
    private String dateFrom;
    @SerializedName("date_to")
    private String dateTo;
    @SerializedName("days")
    private int days;
    @SerializedName("data")
    private List<ListData> listData;
    @SerializedName("health_goals")
    private List<HealthGoal> healthGoals;
    @SerializedName("stats")
    //private BasicTotalStats stats;
    private JsonObject stats;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public int getDays() {
        return days;
    }

    public List<ListData> getListData() {
        return listData;
    }

    public List<HealthGoal> getHealthGoals() {
        return healthGoals;
    }

    public JsonObject getStats() {
        return stats;
    }
}

package xyz.lbartolic.e_gluko.webservices.api.repositories;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogCreateData;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;
import xyz.lbartolic.e_gluko.webservices.api.ApiClient;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogCreateDataResponse;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogInterface;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogResponse;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.HealthLogsListResponse;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.TotalStatsResponse;

/**
 * Created by asus on 17.08.16..
 */
public class HealthLogRepository {
    private static final HealthLogInterface healthLogInterface = ApiClient.getClient()
            .create(HealthLogInterface.class);

    public HealthLogRepository() {

    }

    public static void getLogsList(String api_key, String date, String sort, final HealthLogsListResponse logsListResponse) {
        Log.d("getlogslist", date + " " + api_key);
        Call<HealthLogsList> call = healthLogInterface.logsList(api_key, date, sort);
        call.enqueue(new Callback<HealthLogsList>() {
            @Override
            public void onResponse(Call<HealthLogsList> call, Response<HealthLogsList> response) {
                if (response.isSuccessful()) {
                    Log.d("MSG", "SUCCESS");
                    HealthLogsList healthLogsList = response.body();
                    //Log.d("TRIP_EVENT", trip.getTripDays().get(0).getEvents().get(0).getChildJson().get("confirmation_code").toString());
                    logsListResponse.onSuccess(healthLogsList);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG asdasd", "ERROR");
                    logsListResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<HealthLogsList> call, Throwable t) {
                logsListResponse.onFailure(t);
            }
        });
    }

    public static void getTotalStats(String api_key, String dateFrom, final TotalStatsResponse totalStatsResponse) {
        Call<TotalStats> call = healthLogInterface.totalStats(api_key, dateFrom);
        call.enqueue(new Callback<TotalStats>() {
            @Override
            public void onResponse(Call<TotalStats> call, Response<TotalStats> response) {
                if (response.isSuccessful()) {
                    Log.d("MSG", "SUCCESS");
                    TotalStats totalStats = response.body();
                    totalStatsResponse.onSuccess(totalStats);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG asdasd", "ERROR");
                    totalStatsResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<TotalStats> call, Throwable t) {
                totalStatsResponse.onFailure(t);
            }
        });
    }

    public static void getCreateData(String api_key, final HealthLogCreateDataResponse healthLogCreateDataResponse) {
        Call<HealthLogCreateData> call = healthLogInterface.createData(api_key);
        call.enqueue(new Callback<HealthLogCreateData>() {
            @Override
            public void onResponse(Call<HealthLogCreateData> call, Response<HealthLogCreateData> response) {
                if (response.isSuccessful()) {
                    Log.d("MSG", "SUCCESS");
                    HealthLogCreateData createData = response.body();
                    healthLogCreateDataResponse.onSuccess(createData);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", "ERROR");
                    healthLogCreateDataResponse.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<HealthLogCreateData> call, Throwable t) {
                healthLogCreateDataResponse.onFailure(t);
            }
        });
    }

    public static void postStore(String api_key, Float value, String logTs, String logTypeKey, Integer labelId, Integer insulinTypeId, final HealthLogResponse healthLogResponse) {
        Call<HealthLog> call = healthLogInterface.store(api_key, value, logTs, logTypeKey, labelId, insulinTypeId);
        call.enqueue(new Callback<HealthLog>() {
            @Override
            public void onResponse(Call<HealthLog> call, Response<HealthLog> response) {
                if (response.isSuccessful()) {
                    Log.d("MSG", "SUCCESS");
                    HealthLog healthLog = response.body();
                    healthLogResponse.onSuccess(healthLog);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", "ERROR");
                    healthLogResponse.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<HealthLog> call, Throwable t) {
                healthLogResponse.onFailure(t);
            }
        });
    }
}

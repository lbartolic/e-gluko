package xyz.lbartolic.e_gluko.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.activities.LogsActivity;
import xyz.lbartolic.e_gluko.adapters.LogsListListViewAdapter;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.utils.DateUtil;

/**
 * Created by asus on 15.08.16..
 */
public class LogsListDayFragment extends Fragment {
    private LogsListListViewAdapter lvAdapter;
    private ObservableListView lvLogs;
    private SwipeRefreshLayout srlLogsList;
    private HealthLogsList healthLogsList;
    private ListData logsListDay;
    private TextView tvDayDate;
    private int logsListDayPos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_logs_list_day, container, false);

        String logsListDayJson = getArguments().getString("LOGS_LIST_DAY");
        logsListDay = new Gson().fromJson(logsListDayJson, ListData.class);
        logsListDayPos = getArguments().getInt("LOGS_LIST_DAY_POS");
        String healthLogsListJson = getArguments().getString("LOGS_LIST");
        healthLogsList = new Gson().fromJson(healthLogsListJson, HealthLogsList.class); // from PagerAdapter, used in bundles for other activities as Trip details
        /*String nestedPolymorficRelationTest = trip.getTripDays().get(0).getEvents().get(0).getEventChild().getTransportationType().getTypeName();
        Log.d("NESTED_POLYMORFIC", nestedPolymorficRelationTest); // OUTPUT: Flight*/
        initUI(v);
        setEventListeners();
        setEventList();
        return v;
    }

    private void initUI(View v) {
        lvLogs = (ObservableListView) v.findViewById(R.id.lvLogs);
        srlLogsList = (SwipeRefreshLayout) v.findViewById(R.id.srlLogsList);
    }

    private void setEventList() {
        lvAdapter = new LogsListListViewAdapter(getActivity(), logsListDay, healthLogsList);
        lvLogs.setAdapter(lvAdapter);
        LayoutInflater inflater = getLayoutInflater(null);
        View header = inflater.inflate(R.layout.header_list_item_log, lvLogs, false);
        TextView tvDayDate = (TextView) header.findViewById(R.id.tvDayDate);
        String dayDateTxt = DateUtil.formatDateString(logsListDay.getDate(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_LONG);
        final String dayDateTxtFormatted = dayDateTxt.substring(0, 1).toUpperCase() + dayDateTxt.substring(1);
        tvDayDate.setText(dayDateTxtFormatted);
        Button btnStats = (Button) header.findViewById(R.id.btnStats);
        btnStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_period_stats);
                TextView tvDayDate = (TextView) dialog.findViewById(R.id.tvDayDate);
                tvDayDate.setText(dayDateTxtFormatted);
                LinearLayout llPeriodStats = (LinearLayout) dialog.findViewById(R.id.llPeriodStats);
                ((LogsActivity) getActivity()).setBasicTotalStats(llPeriodStats, logsListDay.getStats());

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        lvLogs.addHeaderView(header, null, false);
    }

    private void setEventListeners() {
        srlLogsList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((LogsActivity) getActivity()).refreshData(LogsListDayFragment.this, false);
            }
        });
        lvLogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                final HealthLog healthLog = healthLogsList.getListData().get(logsListDayPos).getHealthLogs().get(i - 1);
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_log_item_touch);
                TextView tvNoOptions = (TextView) dialog.findViewById(R.id.tvNoOptions);
                Button btnDelete = (Button) dialog.findViewById(R.id.btnDelete);

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                if (healthLog.getInsertType() == 1) {
                    btnDelete.setVisibility(View.GONE);
                    tvNoOptions.setVisibility(View.VISIBLE);
                }
                else {
                    btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            deleteLog(healthLog, i);
                        }
                    });
                }
                dialog.show();
            }
        });
        lvLogs.setOnScrollListener(new AbsListView.OnScrollListener() {
            int prevVisibleItem = 0;
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int i1, int i2) {
                if (prevVisibleItem != firstVisibleItem) {
                    if (prevVisibleItem < firstVisibleItem) {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewLog)).hide();
                    } else {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewLog)).show();
                    }
                }
                prevVisibleItem = firstVisibleItem;
            }
        });
    }

    private void deleteLog(final HealthLog healthLog, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        ((TextView) dialog.findViewById(R.id.tvTitle)).setText(R.string.action_delete);
        String dateTime = DateUtil.formatDateString(healthLog.getLogTs(), DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_DATE_TIME_FORMAT);
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText(healthLog.getLogType().getTypeName() + " (" + dateTime + ")");
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*EventRepository.destroyEvent(User.getAuthUserApiToken(getActivity()), trip.getId(),
                        tripDay.getId(), event.getId(), new EventResponse() {
                            @Override
                            public void onSuccess(Event event) {
                                dialog.hide();
                                ((TripPlanActivity) getActivity()).refreshData(trip, null, true);
                            }

                            @Override
                            public void onError(int status_code) {
                                dialog.hide();
                            }

                            @Override
                            public void onFailure(Throwable throwable) {
                                dialog.hide();
                            }
                        });*/
            }
        });
        dialog.show();
    }

    public void stopSwipeRefresh() {
        srlLogsList.post(new Runnable() {
            @Override
            public void run() {
                srlLogsList.setRefreshing(false);
            }
        });
    }
}

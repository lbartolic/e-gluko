package xyz.lbartolic.e_gluko.models.Statistics.Glucose;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.StatsDescriptiveGoalDiff;
import xyz.lbartolic.e_gluko.models.Statistics.StatsItem;
import xyz.lbartolic.e_gluko.utils.StatsFormatUtil;

/**
 * Created by asus on 06.09.17..
 */
public class GlucoseBeforeAfter {
    @SerializedName("after_count")
    private int afterCount;
    @SerializedName("after_high_percentage")
    private float afterHighPercentage;
    @SerializedName("after_low_percentage")
    private float afterLowPercentage;
    @SerializedName("after_normal_percentage")
    private float afterNormalPercentage;
    @SerializedName("before_count")
    private int beforeCount;
    @SerializedName("before_high_percentage")
    private float beforeHighPercentage;
    @SerializedName("before_low_percentage")
    private float beforeLowPercentage;
    @SerializedName("before_normal_percentage")
    private float beforeNormalPercentage;
    @SerializedName("avg_after")
    private StatsDescriptiveGoalDiff avgAfter;
    @SerializedName("avg_before")
    private StatsDescriptiveGoalDiff avgBefore;
    @SerializedName("max_after")
    private StatsDescriptiveGoalDiff maxAfter;
    @SerializedName("max_before")
    private StatsDescriptiveGoalDiff maxBefore;
    @SerializedName("min_after")
    private StatsDescriptiveGoalDiff minAfter;
    @SerializedName("min_before")
    private StatsDescriptiveGoalDiff minBefore;

    public List<StatsItem> getStatsBeforeItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, avgBefore, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_glucose)),
                new StatsItem(null, maxBefore, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.max_glucose)),
                new StatsItem(null, minBefore, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.min_glucose)),
                new StatsItem(beforeHighPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.high_perc_glucose)),
                new StatsItem(beforeLowPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.low_perc_glucose)),
                new StatsItem(beforeNormalPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.normal_perc_glucose))
        );
    }

    public List<StatsItem> getStatsAfterItems(Context context) {
        return Arrays.asList(
                new StatsItem(null, avgAfter, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.avg_glucose)),
                new StatsItem(null, maxAfter, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.max_glucose)),
                new StatsItem(null, minAfter, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_DEFAULT, context.getString(R.string.min_glucose)),
                new StatsItem(afterHighPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.high_perc_glucose)),
                new StatsItem(afterLowPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.low_perc_glucose)),
                new StatsItem(afterNormalPercentage, null, HealthLogType.TYPE_GLUCOSE, StatsFormatUtil.FORMAT_PERCENTAGE, context.getString(R.string.normal_perc_glucose))
        );
    }

    public int getAfterCount() {
        return afterCount;
    }

    public float getAfterHighPercentage() {
        return afterHighPercentage;
    }

    public float getAfterLowPercentage() {
        return afterLowPercentage;
    }

    public float getAfterNormalPercentage() {
        return afterNormalPercentage;
    }

    public int getBeforeCount() {
        return beforeCount;
    }

    public float getBeforeHighPercentage() {
        return beforeHighPercentage;
    }

    public float getBeforeLowPercentage() {
        return beforeLowPercentage;
    }

    public float getBeforeNormalPercentage() {
        return beforeNormalPercentage;
    }

    public StatsDescriptiveGoalDiff getAvgAfter() {
        return avgAfter;
    }

    public StatsDescriptiveGoalDiff getAvgBefore() {
        return avgBefore;
    }

    public StatsDescriptiveGoalDiff getMaxAfter() {
        return maxAfter;
    }

    public StatsDescriptiveGoalDiff getMaxBefore() {
        return maxBefore;
    }

    public StatsDescriptiveGoalDiff getMinAfter() {
        return minAfter;
    }

    public StatsDescriptiveGoalDiff getMinBefore() {
        return minBefore;
    }
}

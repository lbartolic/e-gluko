package xyz.lbartolic.e_gluko.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import xyz.lbartolic.e_gluko.fragments.LogsListDayFragment;
import xyz.lbartolic.e_gluko.fragments.TotalStatsTypeFragment;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;
import xyz.lbartolic.e_gluko.utils.DateUtil;
import xyz.lbartolic.e_gluko.utils.JsonParseUtil;

/**
 * Created by asus on 15.08.16..
 */
public class TotalStatsPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private TotalStats totalStats;
    private List<String> types = HealthLogType.getAllTypes();

    public TotalStatsPagerAdapter(FragmentManager fm, Context ctx, TotalStats totalStats) {
        super(fm);
        this.context = ctx;
        this.totalStats = totalStats;
    }

    @Override
    public Fragment getItem(int position) {
        TotalStatsTypeFragment tstf = new TotalStatsTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("STATS_TYPE", types.get(position));
        bundle.putInt("STATS_POS", position);
        bundle.putString("TOTAL_STATS", JsonParseUtil.convertObjectToJson(totalStats));
        tstf.setArguments(bundle);
        return tstf;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch(types.get(position)) {
            case HealthLogType.TYPE_GLUCOSE:
                title = "Razina glukoze";
                break;
            case HealthLogType.TYPE_INSULIN:
                title = "Doze inzulina";
                break;
            case HealthLogType.TYPE_PHYSICAL_ACTIVITY:
                title = "Fizička aktivnost";
                break;
            case HealthLogType.TYPE_SLEEP:
                title = "San";
                break;
            case HealthLogType.TYPE_STEP:
                title = "Koraci";
                break;
            case HealthLogType.TYPE_CALORIE:
                title = "Kalorije";
                break;
        }
        return title;
    }

    @Override
    public int getCount() {
        return types.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

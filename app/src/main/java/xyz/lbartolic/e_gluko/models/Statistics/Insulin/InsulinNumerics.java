package xyz.lbartolic.e_gluko.models.Statistics.Insulin;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 06.09.17..
 */
public class InsulinNumerics {
    @SerializedName("sum")
    private float sum;
    @SerializedName("count")
    private int count;
    @SerializedName("avg")
    private float avg;

    public float getSum() {
        return sum;
    }

    public int getCount() {
        return count;
    }

    public float getAvg() {
        return avg;
    }
}

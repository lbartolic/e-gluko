package xyz.lbartolic.e_gluko.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.models.User;
import xyz.lbartolic.e_gluko.utils.JsonParseUtil;
import xyz.lbartolic.e_gluko.webservices.api.interfaces.UserResponse;
import xyz.lbartolic.e_gluko.webservices.api.repositories.UserRepository;

public class LoginActivity extends AppCompatActivity {
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogIn;
    private Button btnSignIn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
        initListeners();
    }

    private void initUI() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogIn = (Button) findViewById(R.id.btnLogIn);
    }

    private void initListeners() {
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String[] credentials = {username, password};
                attemptUserAuth(credentials);
            }
        });
    }

    private void attemptUserAuth(String[] credentials) {
        progressDialog = ProgressDialog.show(this, null, "Prijava u tijeku", true, false);
        UserRepository.authenticateUser(credentials, new UserResponse() {
            @Override
            public void onSuccess(User user) {
                progressDialog.dismiss();
                String userJson = JsonParseUtil.convertObjectToJson(user);
                Log.d("RETROFIT_USER", userJson);
                if (user.getApiToken() == null) {
                    showMessage();
                }
                else handleSuccessfulAuth(user);
            }
            @Override
            public void onError(int code) {
                progressDialog.dismiss();
                Log.d("LOGIN_ERR", code + "");
                showMessage();
            }
            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                Log.d("LOGIN_ERR", t.toString() + "");
                showMessage();
            }
        });
    }

    private void showMessage() {
        final Dialog messageDialog = new Dialog(this);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.dialog_message);
        Button btnClose = (Button) messageDialog.findViewById(R.id.btnClose);
        final TextView tvText = (TextView) messageDialog.findViewById(R.id.tvText);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        tvText.setText("Došlo je do pogreške. Pokušajte ponovo.");
        messageDialog.show();
    }

    private void handleSuccessfulAuth(User user) {
        User.setUserPrefs(this, user);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
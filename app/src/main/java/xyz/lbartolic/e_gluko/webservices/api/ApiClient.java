package xyz.lbartolic.e_gluko.webservices.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by asus on 13.08.16..
 */
public class ApiClient {
    //private static final String BASE_URL = "http://tpapi.projects.lbartolic.xyz/api/";
    private static final String BASE_URL = "http://192.168.5.15:8000/api/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

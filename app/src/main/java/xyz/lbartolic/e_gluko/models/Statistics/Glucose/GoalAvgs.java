package xyz.lbartolic.e_gluko.models.Statistics.Glucose;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 06.09.17..
 */
public class GoalAvgs {
    @SerializedName("max_a1c")
    private float maxA1c;
    @SerializedName("max_after")
    private float maxAfter;
    @SerializedName("max_before")
    private float maxBefore;
    @SerializedName("min_before")
    private float minBefore;

    public float getMaxA1c() {
        return maxA1c;
    }

    public float getMaxAfter() {
        return maxAfter;
    }

    public float getMaxBefore() {
        return maxBefore;
    }

    public float getMinBefore() {
        return minBefore;
    }
}

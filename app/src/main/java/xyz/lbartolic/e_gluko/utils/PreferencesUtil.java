package xyz.lbartolic.e_gluko.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by asus on 02.09.17..
 */
public class PreferencesUtil {
    private static final String PREFERENCE_KEY = "egluko";

    public static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(PREFERENCE_KEY, context.MODE_PRIVATE);
    }


}


package xyz.lbartolic.e_gluko.webservices.api.interfaces;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogCreateData;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;

/**
 * Created by asus on 17.08.16..
 */
public interface HealthLogInterface {
    @GET("{api_key}/health-logs/{date}/{sort}")
    Call<HealthLogsList> logsList(@Path("api_key") String apiKey, @Path("date") String date, @Path("sort") String sort);

    @GET("{api_key}/health-logs/stats/{date_from}")
    Call<TotalStats> totalStats(@Path("api_key") String apiKey, @Path("date_from") String date);

    @GET("{api_key}/health-logs/create-data")
    Call<HealthLogCreateData> createData(@Path("api_key") String apiKey);

    @FormUrlEncoded
    @POST("{api_key}/health-logs")
    Call<HealthLog> store(@Path("api_key") String apiKey,
                          @Field("value") Float value,
                          @Field("log_ts") String logTs,
                          @Field("log_type_key") String logTypeKey,
                          @Field("health_log_label_id") Integer labelId,
                          @Field("insulin_type_id") Integer insulinTypeId);
}

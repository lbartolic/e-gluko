package xyz.lbartolic.e_gluko.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

import xyz.lbartolic.e_gluko.R;
import xyz.lbartolic.e_gluko.activities.LogsActivity;
import xyz.lbartolic.e_gluko.adapters.GoalDiffStatsAdapter;
import xyz.lbartolic.e_gluko.adapters.LogsListListViewAdapter;
import xyz.lbartolic.e_gluko.models.HealthLog;
import xyz.lbartolic.e_gluko.models.HealthLogType;
import xyz.lbartolic.e_gluko.models.HealthLogsList.HealthLogsList;
import xyz.lbartolic.e_gluko.models.HealthLogsList.ListData;
import xyz.lbartolic.e_gluko.models.Statistics.Glucose.GlucoseStats;
import xyz.lbartolic.e_gluko.models.Statistics.StatsByType;
import xyz.lbartolic.e_gluko.models.Statistics.StatsItem;
import xyz.lbartolic.e_gluko.models.Statistics.TotalStats;
import xyz.lbartolic.e_gluko.utils.DateUtil;

/**
 * Created by asus on 15.08.16..
 */
public class TotalStatsTypeFragment extends Fragment {
    private String[] glucoseTabs = {"Ukupno", "Prije obroka", "Nakon obroka"};
    private String[] insulinTabs = {"Ukupno", "Bazalni", "Bolus"};
    private LogsListListViewAdapter lvAdapter;
    private ListView lvLogs;
    private SwipeRefreshLayout srlStatsType;
    private TotalStats totalStats;
    private String statsType;
    private int statsPos;
    private GridView gridView;
    private TextView tvStatsTotalLogs;
    private TextView tvStatsLoggedDays;
    private TextView tvStatsDaysDiff;
    private Date dateFrom;
    private Date dateTo;
    private int diffInDays;
    private TabLayout tlStatsTypeCategs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_total_stats_type, container, false);

        String totalStatsJson = getArguments().getString("TOTAL_STATS");
        statsType = getArguments().getString("STATS_TYPE");
        statsPos = getArguments().getInt("STATS_POS");
        totalStats = new Gson().fromJson(totalStatsJson, TotalStats.class);
        dateFrom = DateUtil.convertStringToDate(totalStats.getDateFrom(), DateUtil.DB_DATE_FORMAT);
        dateTo = DateUtil.convertStringToDate(totalStats.getDateTo(), DateUtil.DB_DATE_FORMAT);
        diffInDays = DateUtil.diffInDays(dateFrom, dateTo);
        initUI(v);
        setStats();
        setEventListeners();
        //setEventList();
        return v;
    }

    private void setEventListeners() {
        tlStatsTypeCategs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setStatsTypeCategory(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setStatsTypeCategory(int position) {
        StatsByType statsByType = totalStats.getStatsByType();
        switch (statsType) {
            case HealthLogType.TYPE_GLUCOSE:
                if (position == 0) {
                    setStatsAdapter(statsByType.getGlucose().getStatsItems(getActivity()));
                }
                else if (position == 1) {
                    setStatsAdapter(statsByType.getGlucose().getBeforeAfter().getStatsBeforeItems(getActivity()));
                }
                else {
                    setStatsAdapter(statsByType.getGlucose().getBeforeAfter().getStatsAfterItems(getActivity()));
                }
                break;
            case HealthLogType.TYPE_INSULIN:
                if (position == 0) {
                    setStatsAdapter(statsByType.getInsulin().getTotalStatsItems(getActivity()));
                }
                else if (position == 1) {
                    setStatsAdapter(statsByType.getInsulin().getBasalStatsItems(getActivity()));
                }
                else {
                    setStatsAdapter(statsByType.getInsulin().getBolusStatsItems(getActivity()));
                }
                break;
            default:

                break;
        }
    }

    private void initUI(View v) {
        //lvLogs = (ListView) v.findViewById(R.id.lvLogs);
        //srlStatsType = (SwipeRefreshLayout) v.findViewById(R.id.srlStatsType);
        gridView = (GridView) v.findViewById(R.id.gvGoalDiffStats);
        tvStatsLoggedDays = (TextView) v.findViewById(R.id.tvStatsLoggedDays);
        tvStatsTotalLogs = (TextView) v.findViewById(R.id.tvStatsTotalLogs);
        tvStatsDaysDiff = (TextView) v.findViewById(R.id.tvStatsDaysDiff);
        tlStatsTypeCategs = (TabLayout) v.findViewById(R.id.tlStatsTypeCategs);
    }

    private void setStats() {
        String loggedDays = "";
        String totalLogs = "";
        String totalLogsTxt = "";
        GoalDiffStatsAdapter adapter = null;
        StatsByType statsByType = totalStats.getStatsByType();
        tlStatsTypeCategs.setVisibility(View.GONE);
        switch (statsType) {
            case HealthLogType.TYPE_GLUCOSE:
                loggedDays = String.valueOf(statsByType.getGlucose().getLoggedDays());
                totalLogs = String.valueOf(statsByType.getGlucose().getTotalLogs());
                totalLogsTxt = getActivity().getString(R.string.total_logs_glucose);
                setStatsTypeCategsTabs(glucoseTabs);
                setStatsAdapter(statsByType.getGlucose().getStatsItems(getActivity()));
                tlStatsTypeCategs.setVisibility(View.VISIBLE);
                break;
            case HealthLogType.TYPE_INSULIN:
                loggedDays = String.valueOf(statsByType.getInsulin().getLoggedDays());
                totalLogs = String.valueOf(statsByType.getInsulin().getTotalLogs());
                totalLogsTxt = getActivity().getString(R.string.total_logs_insulin);
                setStatsTypeCategsTabs(insulinTabs);
                setStatsAdapter(statsByType.getInsulin().getTotalStatsItems(getActivity()));
                tlStatsTypeCategs.setVisibility(View.VISIBLE);
                break;
            case HealthLogType.TYPE_PHYSICAL_ACTIVITY:
                break;
            case HealthLogType.TYPE_SLEEP:
                break;
        }
        tvStatsLoggedDays.setText(String.format("%s %s", loggedDays, getActivity().getString(R.string.logged_days).toLowerCase()));
        tvStatsTotalLogs.setText(String.format("%s %s", totalLogs, totalLogsTxt.toLowerCase()));
        tvStatsDaysDiff.setText(String.format("%sd", String.valueOf(diffInDays)));
    }

    private void setStatsAdapter(List<StatsItem> statsItems) {
        gridView.setAdapter(null);
        gridView.setAdapter(new GoalDiffStatsAdapter(getActivity(), statsItems));
    }

    private void setStatsTypeCategsTabs(String[] tabs) {
        for(int i = 0; i < tabs.length; i++) {
            tlStatsTypeCategs.addTab(tlStatsTypeCategs.newTab().setText(tabs[i]), (i == 0));
        }
    }

    /*private void setEventList() {
        lvAdapter = new LogsListListViewAdapter(getActivity(), logsListDay, healthLogsList);
        lvLogs.setAdapter(lvAdapter);
        LayoutInflater inflater = getLayoutInflater(null);
        View header = inflater.inflate(R.layout.header_list_item_log, lvLogs, false);
        TextView tvDayDate = (TextView) header.findViewById(R.id.tvDayDate);
        String dayDateTxt = DateUtil.formatDateString(logsListDay.getDate(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_LONG);
        final String dayDateTxtFormatted = dayDateTxt.substring(0, 1).toUpperCase() + dayDateTxt.substring(1);
        tvDayDate.setText(dayDateTxtFormatted);
        Button btnStats = (Button) header.findViewById(R.id.btnStats);
        btnStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_period_stats);
                TextView tvDayDate = (TextView) dialog.findViewById(R.id.tvDayDate);
                tvDayDate.setText(dayDateTxtFormatted);
                LinearLayout llPeriodStats = (LinearLayout) dialog.findViewById(R.id.llPeriodStats);
                ((LogsActivity) getActivity()).setBasicTotalStats(llPeriodStats, logsListDay.getStats());

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        lvLogs.addHeaderView(header, null, false);
    }

    private void setEventListeners() {
        lvLogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                final HealthLog healthLog = healthLogsList.getListData().get(logsListDayPos).getHealthLogs().get(i - 1);
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_log_item_touch);
                TextView tvNoOptions = (TextView) dialog.findViewById(R.id.tvNoOptions);
                Button btnDelete = (Button) dialog.findViewById(R.id.btnDelete);

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                if (healthLog.getInsertType() == 1) {
                    btnDelete.setVisibility(View.GONE);
                    tvNoOptions.setVisibility(View.VISIBLE);
                }
                else {
                    btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            deleteLog(healthLog, i);
                        }
                    });
                }
                dialog.show();
            }
        });
        lvLogs.setOnScrollListener(new AbsListView.OnScrollListener() {
            int prevVisibleItem = 0;
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int i1, int i2) {
                if (prevVisibleItem != firstVisibleItem) {
                    if (prevVisibleItem < firstVisibleItem) {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewLog)).hide();
                    } else {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewLog)).show();
                    }
                }
                prevVisibleItem = firstVisibleItem;
            }
        });
    }*/


}

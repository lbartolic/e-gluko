package xyz.lbartolic.e_gluko.utils;

/**
 * Created by asus on 08.09.17..
 */
public class StatsFormatUtil {
    public static final String FORMAT_TIME = "time";
    public static final String FORMAT_DEFAULT = "default";
    public static final String FORMAT_PERCENTAGE = "percentage";

    public static String formatStatsValue(Float value, String formatType) {
       if (formatType == FORMAT_TIME) {
            return UnitsUtil.formatHmsFromMillis(value.intValue());
        }
        else if (formatType == FORMAT_PERCENTAGE) {
            return UnitsUtil.getValueFormatted(value) + "%";
        }
        return UnitsUtil.getValueFormatted(value);
    }
}
